//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/14/2018 08:59:26 PM
// Design Name: 
// Module Name: branch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "../defs.v"

module branch(
    //input
    input [31:0]pca4,
    input [31:0]inst,
    input [`JADDR_TYPE_LEN-1:0]jaddr_type,
    input [`ALUOP_TYPE_LEN-1:0]aluop,

    input [31:0]src1op_data,
    input [31:0]src2op_data,


    //output
    output reg[31:0]branch_new_pc,
    output reg is_branch

    );

    
    wire cmp_result,cmp_tmp1_result,cmp_tmp2_result;
    wire [31:0]src1op_data_pre,src2op_data_pre;
    wire op1nor,op2nor,op3rev;
    assign op1nor=(aluop&`ALUOP_TYPE_OP1NOR)?1:0;
    assign op2nor=(aluop&`ALUOP_TYPE_OP2NOR)?1:0;
    assign op3rev=(aluop&`ALUOP_TYPE_OP3REV)?1:0;

    assign src1op_data_pre=op1nor?~src1op_data:src1op_data;
    assign cmp_tmp1_result=op2nor?(src1op_data_pre[31]==1 && src1op_data_pre!=32'hffff_ffff):((src1op_data_pre[31]==1)); 
    assign cmp_tmp2_result=op3rev?~cmp_tmp1_result:cmp_tmp1_result;
    assign cmp_tmp3_result=op3rev?(~(src1op_data==src2op_data)):(src1op_data==src2op_data);

    assign cmp_result=((aluop&`ALUOP_TYPE_EQU))?(cmp_tmp3_result):cmp_tmp2_result;

    always@(*)
    begin
        casez(jaddr_type)
            `JADDR_TYPE_J:
            begin
                branch_new_pc={pca4[31:28],inst[25:0],2'b00};
                is_branch=1'b1;
            end
            `JADDR_TYPE_JR:
            begin
                branch_new_pc=src1op_data;
                is_branch=1'b1;
            end
            `JADDR_TYPE_B:
            begin
                branch_new_pc=pca4+{{14{inst[15]}},inst[15:0],2'b0};
                is_branch=cmp_result;
            end
            default:
            begin
                branch_new_pc=32'b0;
                is_branch=1'b0;
            end
        endcase
    end 

endmodule
