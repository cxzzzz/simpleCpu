`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/25/2018 04:34:26 PM
// Design Name: 
// Module Name: debug_std
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`define STD_TRACE_FILE "std.txt"
`define TEST_TRACE_FILE "test.txt"
`define MIPS_TEST_FILE "mips.txt"

module debug_std;
    reg clk;
    reg resetn;
    wire reset=~resetn;
    
    reg [5:0]int_n_i;
   
    wire inst_sram_en;
    wire [3:0]inst_sram_wen;
    wire [31:0]inst_sram_addr;
    wire [31:2]inst_sram_addr_mycpu;
    wire [31:0]inst_sram_wdata;
    reg [31:0]inst_sram_rdata,inst_sram_rdata_mycpu;
    
    wire data_sram_en;
    wire [3:0]data_sram_wen;
    wire [31:0]data_sram_addr;
    wire [31:0]data_sram_wdata;
    reg [31:0]data_sram_rdata;
    
    wire [31:0]debug_wb_pc,debug_wb_pc_mycpu;
    wire [3:0]debug_wb_rf_wen,debug_wb_wen_mycpu;
    wire [4:0]debug_wb_rf_wnum,debug_wb_wnum_mycpu;
    wire [31:0]debug_wb_rf_wdata,debug_wb_wdata_mycpu;
    
    
 ls132r_top std_cpu(
    .clk(clk),
    .resetn(resetn),
    .int_n_i(int_n_i),
    .inst_sram_en(inst_sram_en),
    .inst_sram_wen(inst_sram_wen),
    .inst_sram_addr(inst_sram_addr),
    .inst_sram_wdata(inst_sram_wdata),
    .inst_sram_rdata(inst_sram_rdata),
    
    .data_sram_en(data_sram_en),
    .data_sram_wen(data_sram_wen),
    .data_sram_addr(data_sram_addr),
    .data_sram_wdata(data_sram_wdata),
    .data_sram_rdata(32'hf0f1f2f3),//data_sram_rdata),
    
    .debug_wb_pc(debug_wb_pc),
    .debug_wb_rf_wen(debug_wb_rf_wen),
    .debug_wb_rf_wnum(debug_wb_rf_wnum),
    .debug_wb_rf_wdata(debug_wb_rf_wdata)
 );
 
 
 cpu_top mycpu(
        .clk(clk),
        .rst(reset),
        .iram_ena_o(),
        .iram_addra_o(inst_sram_addr_mycpu),
        .iram_douta(inst_sram_rdata_mycpu),
        
        .iram_stall(1'b0),
        
        .dram_ena_o(),
        .dram_addra_o(),
        .dram_wea_o(),
        .dram_dina_o(),
        .dram_douta(32'hf0f1f2f3),
        
        .dram_stall(1'b0),
        
        .int(6'b0),
        
        .debug_wb_pc_o(debug_wb_pc_mycpu),
        .debug_wb_wen_o(debug_wb_wen_mycpu),
        .debug_wb_wnum_o(debug_wb_wnum_mycpu),
        .debug_wb_wdata_o(debug_wb_wdata_mycpu)
        
    );
 integer std_trace,mips_file,test_trace;
 reg [31:0]iram[1023:0]; 
 
 
 always@(posedge clk)
 begin
    inst_sram_rdata<=iram[inst_sram_addr[11:2]];
    inst_sram_rdata_mycpu<=iram[inst_sram_addr_mycpu[11:2]];
 end
 
 always
 begin
    #5;
    clk=~clk;
 end
 
 initial
 begin
    $readmemh(`MIPS_TEST_FILE,iram);
    std_trace=$fopen(`STD_TRACE_FILE);
    test_trace=$fopen(`TEST_TRACE_FILE);
    clk=0;
    resetn=0;
    #13;
    resetn=1;
    #4000;
    $fclose(std_trace);
    $fclose(test_trace);
    $finish;

 end
 wire debug_wb_rf_wena=(|debug_wb_rf_wen);
 wire debug_wb_wena_cpu=(|debug_wb_wen_mycpu);
 always@(posedge clk)
 begin
    if(debug_wb_rf_wena && debug_wb_rf_wnum!=0)
    begin
    $display("pc:%h\twen:%h\twnum:%d\twdata:%h",
                debug_wb_pc[11:0],debug_wb_rf_wen,debug_wb_rf_wnum,debug_wb_rf_wdata);
    $fdisplay(std_trace,"pc:%h\twen:%h\twnum:%d\twdata:%h",
            debug_wb_pc[11:0],debug_wb_rf_wen,debug_wb_rf_wnum,debug_wb_rf_wdata); 
    end  
    if(debug_wb_wena_cpu && debug_wb_wnum_mycpu!=0)
    begin
    $fdisplay(test_trace,"pc:%h\twen:%h\twnum:%d\twdata:%h",
              {debug_wb_pc_mycpu[11:2],2'b0},debug_wb_wen_mycpu,debug_wb_wnum_mycpu,debug_wb_wdata_mycpu); 
    end  
    
 end
    
endmodule
