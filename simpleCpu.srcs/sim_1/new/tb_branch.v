`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/16/2018 04:03:26 PM
// Design Name: 
// Module Name: tb_branch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


`define debug_begin \
        $display("test:%s begining",test_type);
`define debug_pass \
        $display("test:%s passed\n\n",test_type);
`define debug_fail \
        begin   \
        $display("error"); \
        $display("pc:%h",pc); \
        $display("inst:%h",inst); \
        $display("op1:%h\top2:%h",src1op_data,src2op_data); \
        $display("npc:%h\tstd_npc:%h\t",branch_new_pc,std_npc); \
        $display("is_branch:%b\tstd_is_branch:%b",is_branch,std_is_branch); \
        $stop;  \
        end


module tb_branch;

    reg [8*7:0] test_type;
    reg [31:0]pc,tmp32;
    reg [31:0]inst;
    reg [`JADDR_TYPE_LEN-1:0]jaddr_type;
    reg [`ALUOP_TYPE_LEN-1:0]aluop;
    reg [31:0]src1op_data,src2op_data;
    
    wire [31:0]branch_new_pc;
    reg [31:0]std_npc;
    wire is_branch;
    reg std_is_branch;
    branch Branch(
        .pc(pc),
        .inst(inst),
        .jaddr_type(jaddr_type),
        .aluop(aluop),

        .src1op_data(src1op_data),
        .src2op_data(src2op_data),
        
        .branch_new_pc(branch_new_pc),
        .is_branch(is_branch)
    );

    integer i;
    initial 
    begin
    //J
        test_type="j";
        `debug_begin;
        jaddr_type=`JADDR_TYPE_J;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            aluop=$random;
            std_npc={pc[31:28],inst[25:0],2'b00};
            std_is_branch=1;
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;

    //JR
        test_type="jr";
        `debug_begin;
        jaddr_type=`JADDR_TYPE_JR;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            aluop=$random;
            src1op_data=$random;
            std_npc=src1op_data;
            std_is_branch=1;
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;

    //beq
        test_type="beq";
        aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_EQU;
        `debug_begin;
        jaddr_type=`JADDR_TYPE_B;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            tmp32=$random;
            src1op_data={tmp32[31],28'b0,tmp32[2:0]};
            tmp32=$random;
            src2op_data={tmp32[31],28'b0,tmp32[2:0]};

            std_npc=pc+{{16{inst[15]}},inst[15:0]};
            std_is_branch=(src1op_data==src2op_data);
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;

    //bne
        test_type="bne";
        aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_EQU|`ALUOP_TYPE_OP3REV;
        `debug_begin;
        jaddr_type=`JADDR_TYPE_B;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            tmp32=$random;
            src1op_data={tmp32[31],28'b0,tmp32[2:0]};
            tmp32=$random;
            src2op_data={tmp32[31],28'b0,tmp32[2:0]};

            std_npc=pc+{{16{inst[15]}},inst[15:0]};
            std_is_branch=(src1op_data!=src2op_data);
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;

    //bGEZ
        test_type="bgez";
        aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_OP3REV|`ALUOP_TYPE_SIGNED;
        `debug_begin;
        jaddr_type=`JADDR_TYPE_B;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            tmp32=$random;
            src1op_data={tmp32[31],29'b0,tmp32[1:0]};

            std_npc=pc+{{16{inst[15]}},inst[15:0]};
            std_is_branch=($signed(src1op_data)>=$signed(32'b0));
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;

    //bGTZ
        test_type="bgtz";
        aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_OP1NOR|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_SIGNED;
        `debug_begin;
        jaddr_type=`JADDR_TYPE_B;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            tmp32=$random;
            src1op_data={tmp32[31],29'b0,tmp32[1:0]};

            std_npc=pc+{{16{inst[15]}},inst[15:0]};
            std_is_branch=($signed(src1op_data)>$signed(32'b0));
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;

    //bLTZ
        test_type="bltz";
        aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED;
        `debug_begin;
        jaddr_type=`JADDR_TYPE_B;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            tmp32=$random;
            src1op_data={tmp32[31],29'b0,tmp32[1:0]};

            std_npc=pc+{{16{inst[15]}},inst[15:0]};
            std_is_branch=($signed(src1op_data)<$signed(32'b0));
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;

    //bLEZ
        test_type="blez";
        aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED|`ALUOP_TYPE_OP1NOR|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_OP3REV;
        `debug_begin;
        jaddr_type=`JADDR_TYPE_B;
        for(i=0;i<200;i=i+1) 
        begin
            inst=$random;
            tmp32=$random;
            pc={tmp32[31:2],2'b0};
            tmp32=$random;
            src1op_data={tmp32[31],29'b0,tmp32[1:0]};

            std_npc=pc+{{16{inst[15]}},inst[15:0]};
            std_is_branch=($signed(src1op_data)<=$signed(32'b0));
            #2;
            if(is_branch!=std_is_branch)
                `debug_fail;
            if(std_is_branch==1)
                if(std_npc!=branch_new_pc)
                    `debug_fail;
        end
        `debug_pass;
             
             
    end


endmodule
