`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/21/2018 10:52:23 AM
// Design Name: 
// Module Name: ext
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ext(
    input[31:0]inst,
    output[31:0]sign_ext,
    output[31:0]unsign_ext,
    output[31:0]shift_ext
    );
    
    assign unsign_ext={16'b0,inst[15:0]};
    assign sign_ext={{16{inst[15]}},inst[15:0]};
    assign shift_ext={27'b0,inst[10:6]};

endmodule
