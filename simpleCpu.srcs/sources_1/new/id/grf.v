module grf(
    //input
    input clk,
    input rst,

    input [4:0]rs_addr,
    input [4:0]rt_addr,

    input [4:0]rd_addr,
    input [31:0]rd_data,
    input rd_wren,

    //output
    output  [31:0]rs_data, 
    output  [31:0]rt_data
    );
    
    integer i;

    reg [31:0]  regs[31:0];

    assign rs_data= regs[rs_addr];
    assign rt_data= regs[rt_addr];

    always@(posedge rst or posedge clk)
    begin
        if(rst)
            for(i=0;i<32;i=i+1)
                regs[i]=32'b0;                 
        else
            if(rd_wren &&  rd_addr!=5'b0)
                regs[rd_addr]<=rd_data;
    end


endmodule
