`include "../defs.v"

module stage_ex(
    input clk,
    input rst,
    input flush,   
    input stall,
    
    input [31:0]pc,
    input is_branchslot,


    input [31:0]src1op_data,src2op_data,dstop_data,

    input [`OPERAND_TYPE_LEN-1:0]
            dstop_type,
            dstop_from_type,
            src1op_type,
            src2op_type,

    input [`OPERAND_ADDR_LEN-1:0]
            dstop_addr,
            dstop_from_addr,
            src1op_addr,
            src2op_addr,

    input [`ALUOP_TYPE_LEN-1:0]aluop,
    input [`MEMOP_TYPE_LEN-1:0]memop,

    input exc_en,
    input [4:0]exc_code,

    //from mem1
    input [`OPERAND_TYPE_LEN-1:0]mem1_dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]mem1_dstop_addr,
    input [31:0]mem1_dstop_data,

    //from mem2
    input [`OPERAND_ADDR_LEN-1:0]mem2_dstop_addr,
    input [`OPERAND_TYPE_LEN-1:0]mem2_dstop_type,
    input [31:0]mem2_dstop_data,
    input mem2_md_valid,


    //from wb
    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]wb_dstop_addr,
    input [31:0]wb_dstop_data,

    //from cp0
    input [31:0]cp0_dstop_data,

    output [`OPERAND_TYPE_LEN-1:0]dstop_type_o,dstop_from_type_o,
    output [`OPERAND_ADDR_LEN-1:0]dstop_addr_o,dstop_from_addr_o,
    output [31:0]dstop_data_o,

    output [`MEMOP_TYPE_LEN-1:0]memop_o,
    output [31:0]alu_result_o,


    output exc_en_o,
    output [4:0]exc_code_o,

    output [31:0]pc_o,
    output is_branchslot_o,
    
    output md_valid_o,
    output md_busy_o
);



//id->ex
reg [31:0]id_ex_src1op_data,id_ex_src2op_data,id_ex_dstop_data;
reg [`OPERAND_TYPE_LEN-1:0]id_ex_src1op_type,id_ex_src2op_type,id_ex_dstop_type,id_ex_dstop_from_type;
reg [`OPERAND_ADDR_LEN-1:0]id_ex_src1op_addr,id_ex_src2op_addr,id_ex_dstop_addr,id_ex_dstop_from_addr;
reg [`ALUOP_TYPE_LEN-1:0]id_ex_aluop;
reg [`MEMOP_TYPE_LEN-1:0]id_ex_memop;

reg id_ex_exc_en;
reg [4:0]id_ex_exc_code;
reg [31:0]id_ex_pc;
reg id_ex_is_branchslot;

//alu
wire [31:0]Alu_result;
wire Alu_exc_en;
wire [4:0]Alu_exc_code;

//multdiv
wire Multdiv_busy;
wire [31:0]Multdiv_dstop_data;
wire Multdiv_mdvalid;



//mux4
wire [31:0]Mux_dstop_data;

//forward
wire [31:0]Forward_src1op_data,Forward_src2op_data,Forward_dstop_data;

assign dstop_type_o=(stall|flush)?`OPERAND_TYPE_NONE:id_ex_dstop_type;
assign dstop_addr_o=id_ex_dstop_addr;
assign dstop_from_type_o=(stall|flush)?`OPERAND_TYPE_NONE:id_ex_dstop_from_type;
assign dstop_from_addr_o=id_ex_dstop_from_addr;
assign dstop_data_o=Mux_dstop_data;

assign memop_o=(stall|flush)?`OPERAND_TYPE_NONE:id_ex_memop;
assign alu_result_o=Alu_result;

assign exc_en_o=(stall|flush)?`OPERAND_TYPE_NONE:(exc_en|Alu_exc_en);
assign exc_code_o=exc_en?id_ex_exc_code:Alu_exc_code;

assign pc_o=id_ex_pc;
assign is_branchslot_o=(stall|flush)?0:id_ex_is_branchslot;

assign md_busy_o=Multdiv_busy;
assign md_valid_o=Multdiv_mdvalid;

always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        id_ex_src1op_data<=0;
        id_ex_src2op_data<=0;
        id_ex_dstop_data<=0;

        id_ex_src1op_type<=0;
        id_ex_src2op_type<=0;
        id_ex_dstop_type<=0;
        id_ex_dstop_from_type<=0;

        id_ex_src1op_addr<=0;
        id_ex_src2op_addr<=0;
        id_ex_dstop_addr<=0;
        id_ex_dstop_from_addr<=0;

        id_ex_aluop<=0;
        id_ex_memop<=0;

        id_ex_exc_en<=0;
        id_ex_exc_code<=0;

        id_ex_pc<=0;
        id_ex_is_branchslot<=0;
    end
    else //clk
    begin
        if(stall);
        else
        begin
            id_ex_src1op_data<=src1op_data;
            id_ex_src2op_data<=src2op_data;
            id_ex_dstop_data<=dstop_data;

            id_ex_src1op_type<=src1op_type;
            id_ex_src2op_type<=src2op_type;
            id_ex_dstop_type<=dstop_type;
            id_ex_dstop_from_type<=dstop_from_type;

            id_ex_src1op_addr<=src1op_addr;
            id_ex_src2op_addr<=src2op_addr;
            id_ex_dstop_addr<=dstop_addr;
            id_ex_dstop_from_addr<=dstop_from_addr;

            id_ex_aluop<=aluop;
            id_ex_memop<=memop;

            id_ex_exc_en<=exc_en;
            id_ex_exc_code<=exc_code;

            id_ex_pc<=pc;
            id_ex_is_branchslot<=is_branchslot;
        end
    end
end

mux5
#(
    .TYPE_LEN(`OPERAND_TYPE_LEN),
    .OP1_TYPE(`OPERAND_TYPE_ALU),
    .OP2_TYPE(`OPERAND_TYPE_HI),
    .OP3_TYPE(`OPERAND_TYPE_LO),
    .OP4_TYPE(`OPERAND_TYPE_CP0)
)
Mux_dstopdata
(
    .op_type(id_ex_dstop_from_type),
    .op1_data(Alu_result),
    .op2_data(Multdiv_dstop_data),
    .op3_data(Multdiv_dstop_data),
    .op4_data(cp0_dstop_data),
    .op5_data(Forward_dstop_data),
    .op_data(Mux_dstop_data)
);



//

alu Alu(
    .aluop(id_ex_aluop),
    .src1op_data(Forward_src1op_data),
    .src2op_data(Forward_src2op_data),
    
    //output
    .result(Alu_result),
    .exc_en(Alu_exc_en),
    .exc_code(Alu_exc_code)
);

multdiv Multdiv(
    .clk(clk),
    .rst(rst),
    .flush(flush),
    .aluop((stall|flush)?`ALUOP_TYPE_NONE:id_ex_aluop),
    
    .src1op_data(Forward_src1op_data),
    .src2op_data(Forward_src2op_data),
    
    .dstop_type(wb_dstop_type),
    .dstop_data(wb_dstop_data),
    .mdvalid(mem2_md_valid),
    
    .dstop_from_type(id_ex_dstop_from_type),
    .busy_o(Multdiv_busy),
    .dstop_data_o(Multdiv_dstop_data),
    .mdvalid_o(Multdiv_mdvalid)
);


//
forward Forward_src1op(
    .srcop_type(`OPERAND_TYPE_REG),
    .srcop_addr(id_ex_src1op_addr),
    .srcop_data_i(id_ex_src1op_data),

    .ex_dstop_type(`OPERAND_TYPE_NONE),
    .ex_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .ex_dstop_data(32'b0),

    .mem1_dstop_type(mem1_dstop_type),
    .mem1_dstop_addr(mem1_dstop_addr),
    .mem1_dstop_data(mem1_dstop_data),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_data(mem2_dstop_data),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_src1op_data)
);

forward Forward_src2op(
    .srcop_type(id_ex_src2op_type),
    .srcop_addr(id_ex_src2op_addr),
    .srcop_data_i(id_ex_src2op_data),

    .ex_dstop_type(`OPERAND_TYPE_NONE),
    .ex_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .ex_dstop_data(0),

    .mem1_dstop_type(mem1_dstop_type),
    .mem1_dstop_addr(mem1_dstop_addr),
    .mem1_dstop_data(mem1_dstop_data),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_data(mem2_dstop_data),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_src2op_data)
);

forward Forward_dstop(
    .srcop_type(id_ex_dstop_from_type),
    .srcop_addr(id_ex_dstop_from_addr),
    .srcop_data_i(id_ex_dstop_data),

    .ex_dstop_type(`OPERAND_TYPE_NONE),
    .ex_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .ex_dstop_data(0),

    .mem1_dstop_type(mem1_dstop_type),
    .mem1_dstop_addr(mem1_dstop_addr),
    .mem1_dstop_data(mem1_dstop_data),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_data(mem2_dstop_data),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_dstop_data)
);
endmodule
