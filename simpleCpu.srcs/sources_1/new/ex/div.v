`define DIV_INIT    0
`define DIV_START   1
`define DIV_WAIT    2
//`define DIV_READ    3

module div(
    input clk,
    input rst,
    input div_en, 
    input div_signed,
    input [31:0]dividend,
    input [31:0]divisor,

    //to mult_div
    output div_busy,
    output [31:0]remainder,
    output [31:0]quotient
);



wire [31:0]abs_dividend=(div_signed&&(dividend[31]))?(~dividend+32'b1):dividend;
wire [31:0]abs_divisor=(div_signed&&(divisor[31]))?(~divisor+32'b1):divisor;

reg  [32:0]abs_quotient,abs_remainder;

reg [3:0]state;

//axi
reg axi_dividend_tvalid,axi_divisor_tvalid;
//wire axi_divident_tready,axi_divisor_tready;
wire [31:0]axi_dividend_tdata,axi_divisor_tdata;

wire axi_dout_tvalid;
wire [63:0]axi_dout_tdata;


assign axi_dividend_tdata=abs_dividend;
assign axi_divisor_tdata=abs_divisor;

assign quotient=(div_signed && dividend[31]!=divisor[31])?//有符号且被除数与除数异号
            (~abs_quotient+32'b1):abs_quotient;
            
assign remainder=(div_signed && dividend[31])? //有符号运算且被除数为负数
                    (~abs_remainder+32'b1):abs_remainder;


assign div_busy=(state!=`DIV_INIT);

div_gen_0 your_instance_name (
  .aclk(clk),                                      // input wire aclk
  .aresetn(~rst),                                // input wire aresetn
  .s_axis_divisor_tvalid(axi_divisor_tvalid),    // input wire s_axis_divisor_tvalid
  .s_axis_divisor_tdata(axi_divisor_tdata),      // input wire [31 : 0] s_axis_divisor_tdata
  .s_axis_dividend_tvalid(axi_dividend_tvalid),  // input wire s_axis_dividend_tvalid
  .s_axis_dividend_tdata(axi_dividend_tdata),    // input wire [31 : 0] s_axis_dividend_tdata
  .m_axis_dout_tvalid(axi_dout_tvalid),          // output wire m_axis_dout_tvalid
  .m_axis_dout_tdata(axi_dout_tdata)            // output wire [63 : 0] m_axis_dout_tdata
);

always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        state<=`DIV_INIT;
        axi_dividend_tvalid<=0;
        axi_divisor_tvalid<=0;
        //div_busy<=0;
    end
    else
    begin
        case (state)
        `DIV_INIT:
            if(div_en)
            begin
                state<=`DIV_START;
                axi_dividend_tvalid<=1;
                axi_divisor_tvalid<=1;
                //div_busy<=1;
            end
        `DIV_START:
//            if(divident_tready && divisor_tready)
            begin
                axi_dividend_tvalid<=0;
                axi_divisor_tvalid<=0;                
                state<=`DIV_WAIT;
            end
        `DIV_WAIT:
        begin
            if(axi_dout_tvalid)
            begin
                abs_quotient<=axi_dout_tdata[63:32];
                abs_remainder<=axi_dout_tdata[31:0];
                //div_busy<=0;
                state<=`DIV_INIT;
            end
        end
        endcase
    end
end

endmodule
