module stage_if1(
    input clk,
    input rst,
    input flush,
    input stall,
    
    input is_branch,
    input is_exception,

    input [31:0]branch_new_pc,
    input [31:0]exception_new_pc,

    
    output [31:0]pc_o,
    output [31:0]pca4_o,
    
    output iram_ena_o,
    output [31:2]iram_addra_o

);

wire [31:0]Pc_pc,Pc_pca4; 

assign pc_o=Pc_pc;
assign pca4_o=Pc_pca4;

assign iram_ena_o=1;
assign iram_addra_o=Pc_pc[31:2];

pc
 #(.INITIAL_PC(32'hBF00_0000))
 Pc(
    //input
    .clk(clk),
    .rst(rst),
    .stall(stall),
    .is_branch(is_branch),
    .is_exception(is_exception),
    .branch_new_pc(branch_new_pc),
    .exception_new_pc(exception_new_pc),

    //output
    .pc(Pc_pc),
    .pca4(Pc_pca4)

    );

endmodule
