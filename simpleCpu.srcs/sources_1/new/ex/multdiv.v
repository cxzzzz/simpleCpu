`include "../defs.v"
`define MD_INIT 0
`define MD_WAIT 1
//`define MD_WRITE 2

`define MD_VALID   1
`define MD_INVALID 2

module multdiv(
    //input
    input clk,
    input rst,
    input flush,
    
    input [`ALUOP_TYPE_LEN-1:0]aluop,

    input [31:0]src1op_data,
    input [31:0]src2op_data,


    
    
    //input from wb
    input [`OPERAND_TYPE_LEN-1:0]dstop_type,
    input [31:0]dstop_data,
    
    input mdvalid, 

    //output  in ex
    input [`OPERAND_TYPE_LEN-1:0]dstop_from_type,
    output busy_o,
    output [31:0]dstop_data_o,
    output mdvalid_o
    );
    



reg [31:0] HI,LO;
reg [1:0]state,vstate;

reg [31:0]r_src1op_data,r_src2op_data;
wire [31:0]w_src1op_data,w_src2op_data;
reg [`ALUOP_TYPE_LEN-1:0]r_aluop;
wire [`ALUOP_TYPE_LEN-1:0]w_aluop;

wire [63:0]tmp_result,result;
reg r_mdvalid;

//mult
wire mult_en,mult_signed;
wire mult_busy;
wire [31:0]mult_prodHI,mult_prodLO;

//div
wire div_en,div_signed;
wire div_busy;
wire [31:0]div_remainder,div_quotient;


assign mult_en=((aluop&`ALUOP_TYPE_BASICOP_MASK)==`ALUOP_TYPE_MULT);
assign div_en=((aluop&`ALUOP_TYPE_BASICOP_MASK)==`ALUOP_TYPE_DIV);

assign mult_signed=((w_aluop&`ALUOP_TYPE_SIGNED)!=0);
assign div_signed=((w_aluop&`ALUOP_TYPE_SIGNED)!=0);

wire md_en=mult_en|div_en;
wire md_busy=mult_busy|div_busy;

assign  busy_o= md_en|(state!=`MD_INIT);


//save s1_data,s2_data
//save aluop

assign w_src1op_data=md_en?src1op_data:r_src1op_data;
assign w_src2op_data=md_en?src2op_data:r_src2op_data;
assign w_aluop=md_en?aluop:r_aluop;

assign tmp_result=((w_aluop&`ALUOP_TYPE_BASICOP_MASK) ==`ALUOP_TYPE_MULT)?
            {mult_prodHI,mult_prodLO}:{div_remainder,div_quotient};
assign result=((w_aluop&`ALUOP_TYPE_MDADD)!=0)?({HI,LO}+{tmp_result}):
                ((w_aluop&`ALUOP_TYPE_MDSUB)!=0)?({HI,LO}-{tmp_result}):
                    {tmp_result};


//mfhi,mflo
assign mdvalid_o=mult_en|div_en;
assign dstop_data_o=(dstop_from_type==`OPERAND_TYPE_HI)?HI:LO;





always@(posedge clk)
begin
    if(md_en)
    begin
        r_aluop<=aluop;
        r_src1op_data<=src1op_data;
        r_src2op_data<=src2op_data;
    end
end



/*always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        state<=`MD_INIT;
        HI<=0;
        LO<=0;
        r_mdvalid<=0;
    end
    else
    begin
        if(state==`MD_INIT)
        begin
            if(md_en)
                state<=`MD_WAIT;
            else
            begin  //mthi //mtlo
                if(dstop_type==`OPERAND_TYPE_HI)
                    HI<=dstop_data;
                else if(dstop_type==`OPERAND_TYPE_LO)
                    LO<=dstop_data;
            end
        end
        else  //MD_WAIT  //mult div
        begin
            if(mdvalid)
                r_mdvalid<=1;
            if(!(mult_busy || div_busy)) //finished
            begin
                if(r_mdvalid){HI,LO}<=result;
                state<=`MD_INIT;
            end
       end
    end
end*/

//vstate
always@(posedge clk or posedge rst)
begin
    if(rst)vstate<=`MD_INIT;
    else //clk
    case (vstate)
       `MD_INIT:
       begin
            if(flush)vstate<=`MD_INVALID;
            else if(mdvalid)vstate<=`MD_VALID;
       end
       default:
       begin
            if(md_en)vstate<=`MD_INIT;
       end
    endcase
end


//state
always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        state<=`MD_INIT;
    end
    else
    begin
        case (state)
            `MD_INIT:
                if(md_en)state<=`MD_WAIT;
            `MD_WAIT:
                if(!(mult_busy || div_busy) && vstate!=`MD_INIT) //finished
                    state<=`MD_INIT;
            default:
                state<=`MD_INIT;
        endcase
    end
end

//write to hi,lo
always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        HI<=0;
        LO<=0;
    end
    else if(dstop_type==`OPERAND_TYPE_HI)
        HI<=dstop_data;
    else if(dstop_type==`OPERAND_TYPE_LO)
        LO<=dstop_data;
    else if(state==`MD_WAIT && !(mult_busy|div_busy) && vstate==`MD_VALID)
        {HI,LO}=result;
end

mult Mult(
    .clk(clk),
    .rst(rst),
    .mult_en(mult_en),
    .mult_signed(mult_signed),
    .A(w_src1op_data),
    .B(w_src2op_data),

    .mult_busy(mult_busy),
    .prodHI(mult_prodHI),
    .prodLO(mult_prodLO)
);
div Div(
    .clk(clk),
    .rst(rst),
    .div_en(div_en),
    .div_signed(div_signed),
    .dividend(w_src1op_data),
    .divisor(w_src2op_data),
    
    .div_busy(div_busy),
    .remainder(div_remainder),
    .quotient(div_quotient)
    );


endmodule
