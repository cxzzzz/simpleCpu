`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/14/2018 02:58:41 PM
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "../defs.v"

module alu(

    //input
    input [`ALUOP_TYPE_LEN-1:0]aluop,
    input [31:0]src1op_data,
    input [31:0]src2op_data,

    //output
    output reg[31:0]result,
    output exc_en,
    output [4:0]exc_code

    );

    wire src1op_add_pre_32b,src2op_add_pre_32b;
    wire [31:0] src1op_add_pre,src2op_add_pre;
    assign {src1op_add_pre_32b,src1op_add_pre}=((aluop&`ALUOP_TYPE_OP1NOR )!=0)?{~src1op_data[31],~src1op_data}:{src1op_data[31],src1op_data};
    assign {src2op_add_pre_32b,src2op_add_pre}=((aluop&`ALUOP_TYPE_OP2NOR )!=0)?{~src2op_data[31],~src2op_data}:{src2op_data[31],src2op_data};

    //add 
    wire [31:0] add_result,src2op_add_pre2;
    wire  add_carry_bit;
    wire  add_exception;
    
   // assign src2op_add_pre2=src2op_add_pre+ ;

    
    assign {add_carry_bit,add_result}={src1op_add_pre_32b,src1op_add_pre}+{src2op_add_pre_32b,src2op_add_pre}+(((aluop&`ALUOP_TYPE_ADDONE)!=0)?1:0);
    assign overflow=(add_carry_bit!=add_result[31]);


    //cmp
    wire [31:0] src1op_cmp_pre;
    wire [31:0] src2op_cmp_pre;
    wire cmp_tmp_result;
    wire [31:0]cmp_result;

    assign src1op_cmp_pre=(aluop&`ALUOP_TYPE_SIGNED)?
            ({~src1op_add_pre[31],src1op_add_pre[30:0]}):src1op_add_pre;
    assign src2op_cmp_pre=(aluop&`ALUOP_TYPE_SIGNED)?
            ({~src2op_add_pre[31],src2op_add_pre[30:0]}):src2op_add_pre;


    assign cmp_tmp_result=src1op_cmp_pre < src2op_cmp_pre;
    assign cmp_result={31'b0,((aluop&`ALUOP_TYPE_OP3REV)?~cmp_tmp_result:cmp_tmp_result)};

    /*
    //mul
    wire [63:0]mult_result,mult_signed_result,mult_unsigned_result;
    assign mult_signed_result=($signed)src1op_data * ($sigend)src2op_data;
    assign mult_signed_result=($unsigned)src1op_data * ($unsigend)src2op_data;
    */

   //logic

   //basic_logic
   reg [31:0] logic_result;
   wire [31:0] xor_result,lui_result,basic_logic_tmp_result,basic_logic_result;

   assign basic_logic_tmp_result = src1op_add_pre | src2op_add_pre;
   assign basic_logic_result = ((aluop&`ALUOP_TYPE_OP3REV)==0)?basic_logic_tmp_result:~basic_logic_tmp_result;
   //xor
   assign xor_result=src1op_data ^ src2op_data;
   //lui
   assign lui_result={src2op_data[15:0],16'b0};


   //shift
   wire [31:0] shift_left_result;
   wire [31:0] shift_right_result;
   wire [31:0] sll_result,srl_result,sra_result;
   wire [`ALUOP_TYPE_LEN-1:0]aluop_basic;
   //sll
    assign sll_result=src1op_data << src2op_data[4:0];

    //srl
    assign srl_result=src1op_data >> src2op_data[4:0];

    //sra
    assign sra_result=(($signed(src1op_data)) >>> (src2op_data[4:0]));

    assign shift_left_result=sll_result;
    assign shift_right_result=((`ALUOP_TYPE_SHIFT_ARITHMETIC& aluop)!=0)?
        sra_result:srl_result;

    assign aluop_basic={`ALUOP_TYPE_EXTOP_LEN'b0,aluop[`ALUOP_TYPE_BASICOP_LEN-1:0]};

   

    always@(*)
    begin
        result=0;
        case(aluop_basic)
            `ALUOP_TYPE_ADD:
                result=add_result;
            `ALUOP_TYPE_CMP:
                result=cmp_result;
            `ALUOP_TYPE_LUI:
                result=lui_result;
            `ALUOP_TYPE_XOR:
                result=xor_result;
            `ALUOP_TYPE_OR:
                result=basic_logic_result;
            `ALUOP_TYPE_SHIFT_LEFT:
                result=shift_left_result;
            `ALUOP_TYPE_SHIFT_RIGHT:
                result=shift_right_result;
        endcase
    end

    assign exc_en= overflow && ((`ALUOP_TYPE_EXCEPTION & aluop)!=0);
    assign exc_code=`EXCEPTION_TYPE_OV;

endmodule
