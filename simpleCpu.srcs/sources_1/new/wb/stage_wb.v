`include "../defs.v"

module stage_wb(
    input clk,
    input rst,
    input flush,
    input stall,


    //from mem2
    input [31:0]dstop_data,
    input [`OPERAND_TYPE_LEN-1:0]dstop_from_type,
    input [`OPERAND_TYPE_LEN-1:0]dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]dstop_addr,
    
    input [31:0]pc,
    //input [`MEMOP_TYPE_LEN-1:0]memop,

    input exc_en,
    input [4:0]exc_code,


    //to id
    output [`OPERAND_TYPE_LEN-1:0]dstop_type_o,
    output [`OPERAND_TYPE_LEN-1:0]dstop_from_type_o,
    output [`OPERAND_ADDR_LEN-1:0]dstop_addr_o,
    output [31:0]dstop_data_o,



    //to forward
    output [31:0]mem2_wb_dstop_data_o,
    
    //to debug
    output [31:0]debug_wb_pc_o,
    output [3:0]debug_wb_wen_o,
    output [4:0]debug_wb_wnum_o,
    output [31:0]debug_wb_wdata_o
    

        
); 
//mem2->wb
reg [31:0]mem2_wb_dstop_data;
reg [`OPERAND_TYPE_LEN-1:0]mem2_wb_dstop_type,mem2_wb_dstop_from_type;
reg [`OPERAND_ADDR_LEN-1:0]mem2_wb_dstop_addr;

reg [31:0]mem2_wb_pc;

assign dstop_type_o=(stall|flush)?`OPERAND_TYPE_NONE:mem2_wb_dstop_type;
assign dstop_addr_o=mem2_wb_dstop_addr;
assign dstop_data_o=mem2_wb_dstop_data;
assign mem2_wb_dstop_data_o=mem2_wb_dstop_data;

assign dstop_from_type_o=mem2_wb_dstop_from_type;

assign debug_wb_pc_o=mem2_wb_pc;
assign debug_wb_wen_o=(mem2_wb_dstop_type==`OPERAND_TYPE_REG)?4'b1111:4'b0000;
assign debug_wb_wnum_o=mem2_wb_dstop_addr[4:0];
assign debug_wb_wdata_o=mem2_wb_dstop_data;

always@(posedge clk or posedge rst )
begin
    if(rst )
    begin
        mem2_wb_dstop_data<=0;
        mem2_wb_dstop_type<=0;
        mem2_wb_dstop_addr<=0;
        mem2_wb_dstop_from_type<=0;
        mem2_wb_pc<=0;
        
        //mem2_wb_exc_code<=exc_code;
        //mem2_wb_exc_en<=exc_en;
    end
    else
    begin
        if(stall);
        else
        begin
            mem2_wb_dstop_data<=dstop_data;
            mem2_wb_dstop_type<=dstop_type;
            mem2_wb_dstop_addr<=dstop_addr;
            mem2_wb_dstop_from_type<=dstop_from_type;
            mem2_wb_pc<=pc;

            //mem2_wb_exc_code<=exc_code;
            //mem2_wb_exc_en<=exc_en;
        end

    end
end


endmodule
