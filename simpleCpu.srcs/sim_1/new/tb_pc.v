`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/24/2018 01:42:39 PM
// Design Name: 
// Module Name: tb_pc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define debug_pass begin   \
        $display("pass");\
        $stop; end     

`define debug_fail begin   \
        $display("error:\tstd_pc:%h\tpc:%h",std_pc,pc);\
        $stop; end     

`define debug_assert(x) begin \
        if(!(x))`debug_fail;   \
        end;


module tb_pc;

reg clk,rst,stall;

reg is_branch,is_exception;

reg [31:0]branch_new_pc;
reg [31:0]exception_new_pc;

wire [31:0]pc;
wire [31:0]pca4;

reg [31:0]std_pc,old_pc;
wire [31:0]std_pca4=std_pc+4;

reg check;
pc
#(
    .INITIAL_PC(32'h00001000)
)
Pc(
    .clk(clk),
    .rst(rst),
    .stall(stall),
    
    .is_branch(is_branch),
    .is_exception(is_exception),
    
    .branch_new_pc(branch_new_pc),
    .exception_new_pc(exception_new_pc),
    
    .pc(pc),
    .pca4(pca4)
);


always
begin
    #5;
    clk=~clk;
end

integer i;
initial
begin
    rst=0;
    clk=0;
    is_branch=0;
    is_exception=0;
    std_pc=32'h00001000;
    stall=0;
    
    #13;
    rst=0;
    #5;
    `debug_assert( std_pc==pc);
    `debug_assert( std_pca4==pca4);
    
    
    
    for(i=0;i<100;i=i+1)
    begin
        is_branch=$random()%2;
        is_exception=$random()%2;
        stall=(($random()%4)==0);
        branch_new_pc=$random()<<2;
        exception_new_pc=$random()<<2;
        if(stall)std_pc=std_pc;
        else if(is_exception)std_pc=exception_new_pc;
        else if(is_branch)std_pc=branch_new_pc;
        else std_pc=std_pc+4;
        #10;
        `debug_assert(std_pc==pc);
        `debug_assert(std_pca4==pca4);
    end
    
    `debug_pass;
    
end


endmodule
