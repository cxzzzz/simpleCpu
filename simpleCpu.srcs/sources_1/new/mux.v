`include "defs.v"

module mux2
#(  parameter   TYPE_LEN=0,
                OP1_TYPE=0
                //OP2_TYPE=0
)
(
    input [TYPE_LEN-1:0]op_type,
    input [31:0]op1_data,op2_data,
    output [31:0]op_data
);
    assign op_data= (op_type==OP1_TYPE)?op1_data:op2_data;
endmodule

module mux3
#(  parameter   TYPE_LEN=0,
                OP1_TYPE=0,
                OP2_TYPE=0
)
(
    input [TYPE_LEN-1:0]op_type,
    input [31:0]op1_data,op2_data,op3_data,
    output [31:0]op_data
);

    assign op_data= (op_type==OP1_TYPE)?op1_data:((op_type==OP2_TYPE)?op2_data:op3_data);
endmodule

module mux4
#(  parameter   TYPE_LEN=0, 
                OP1_TYPE=0,
                OP2_TYPE=0,
                OP3_TYPE=0
)
(
    input [TYPE_LEN-1:0]op_type,
    input [31:0]op1_data,op2_data,op3_data,op4_data,
    output reg[31:0]op_data
);

    always@(*)
    begin
        case(op_type)
            OP1_TYPE:
                op_data=op1_data;
            OP2_TYPE:
                op_data=op2_data;
            OP3_TYPE:
                op_data=op3_data;
            default:
                op_data=op4_data;
        endcase
    end

    //assign op_data= (op_type==OP1_TYPE)?op1_data:((op_type==OP2_TYPE)?op2_data:op3_data);
endmodule

module mux5
#(  parameter   TYPE_LEN=0, 
                OP1_TYPE=0,
                OP2_TYPE=0,
                OP3_TYPE=0,
                OP4_TYPE=0
)
(
    input [TYPE_LEN-1:0]op_type,
    input [31:0]op1_data,op2_data,op3_data,op4_data,op5_data,
    output reg[31:0]op_data
);

    always@(*)
    begin
        case(op_type)
            OP1_TYPE:
                op_data=op1_data;
            OP2_TYPE:
                op_data=op2_data;
            OP3_TYPE:
                op_data=op3_data;
            OP4_TYPE:
                op_data=op4_data;
            default:
                op_data=op5_data;
        endcase
    end

    //assign op_data= (op_type==OP1_TYPE)?op1_data:((op_type==OP2_TYPE)?op2_data:op3_data);
endmodule



