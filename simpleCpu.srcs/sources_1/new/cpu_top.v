module cpu_top(
    input clk,
    input rst,

    //iram
    output iram_ena_o, 
    output [31:2]iram_addra_o,

    input [31:0]iram_douta,

    input iram_stall,


    //dram

    output dram_ena_o,
    output [31:2]dram_addra_o,
    output [3:0]dram_wea_o,
    output [31:0]dram_dina_o,

    input [31:0]dram_douta,

    input dram_stall,


    //int

    input [5:0]int,
    
    //debug
    output [31:0]debug_wb_pc_o,
    output [3:0]debug_wb_wen_o,
    output [4:0]debug_wb_wnum_o,
    output [31:0]debug_wb_wdata_o
        
    );


wire stall;

//if1

wire [31:0]If1_pc,If1_pca4;

wire If1_iram_ena;
wire [31:2]If1_iram_addra;

//if2

wire [31:0]If2_inst,If2_pca4,If2_pc;

//id
wire Id_stall;
wire [31:0]Id_src1op_data,Id_src2op_data,Id_dstop_data;
wire [`OPERAND_TYPE_LEN-1:0]Id_dstop_type,Id_dstop_from_type,Id_src2op_type;

wire [`OPERAND_ADDR_LEN-1:0]Id_dstop_addr,Id_dstop_from_addr,Id_src1op_addr,Id_src2op_addr;

wire [`ALUOP_TYPE_LEN-1:0]Id_aluop;
wire [`MEMOP_TYPE_LEN-1:0]Id_memop;

wire [4:0]Id_exc_code;
wire Id_exc_en;

wire Id_is_branch;
wire [31:0]Id_branch_new_pc;

wire [31:0]Id_pc;
wire Id_is_branchslot;

//ex

wire [31:0]Ex_dstop_data,Ex_alu_result;
wire [`OPERAND_TYPE_LEN-1:0]Ex_dstop_type,Ex_dstop_from_type;

wire [`OPERAND_ADDR_LEN-1:0]Ex_dstop_addr,Ex_dstop_from_addr;

wire [`MEMOP_TYPE_LEN-1:0]Ex_memop;

wire [4:0]Ex_exc_code;
wire Ex_exc_en;

wire [31:0]Ex_pc;
wire Ex_is_branchslot;


wire Ex_md_valid;
wire Ex_md_busy;

//mem1
wire [31:0]Mem1_dstop_data,Mem1_alu_result;
wire [`OPERAND_TYPE_LEN-1:0]Mem1_dstop_type,Mem1_dstop_from_type;
wire [`OPERAND_ADDR_LEN-1:0]Mem1_dstop_addr,Mem1_dstop_from_addr;
wire [`MEMOP_TYPE_LEN-1:0]Mem1_memop;

wire [4:0]Mem1_exc_code;
wire Mem1_exc_en;

wire [31:0]Mem1_ex_mem1_dstop_data;

wire [31:0]Mem1_pc;
wire Mem1_is_branchslot;

wire Mem1_dram_ena;
wire [3:0]Mem1_dram_wea;
wire [31:2]Mem1_dram_addra;
wire [31:0]Mem1_dram_dina;

wire Mem1_md_valid;

//mem2
wire [31:0]Mem2_dstop_data;
wire [`OPERAND_TYPE_LEN-1:0]Mem2_dstop_type,Mem2_dstop_from_type;
wire [`OPERAND_ADDR_LEN-1:0]Mem2_dstop_addr;

wire [4:0]Mem2_exc_code;
wire Mem2_exc_en;

wire [31:0]Mem2_mem1_mem2_dstop_data;

wire [31:0]Mem2_pc;
wire Mem2_is_branchslot;

wire Mem2_md_valid;
//wb
wire [31:0]Wb_dstop_data;
wire [`OPERAND_TYPE_LEN-1:0]Wb_dstop_type,Wb_dstop_from_type;
wire [`OPERAND_ADDR_LEN-1:0]Wb_dstop_addr;



//cp0
wire Cp0_exc_flush;
wire [31:0]Cp0_dstop_data;
wire [31:0]Cp0_exception_new_pc;
wire Cp0_is_exception;


wire [31:0]Wb_mem2_wb_dstop_data;


assign stall=Id_stall;

assign iram_ena_o=If1_iram_ena;
assign iram_addra_o=If1_iram_addra;

assign dram_ena_o=Mem1_dram_ena;
assign dram_wea_o=Mem1_dram_wea;
assign dram_addra_o=Mem1_dram_addra;
assign dram_dina_o=Mem1_dram_dina;

stage_if1 Stage_if1(
    .clk(clk),
    .rst(rst),
    .stall(Id_stall|iram_stall|dram_stall),
    .flush(Cp0_exc_flush),
    .is_branch(Id_is_branch),
    .is_exception(Cp0_is_exception),
    .branch_new_pc(Id_branch_new_pc),
    .exception_new_pc(Cp0_exception_new_pc),
    .pc_o(If1_pc),
    .pca4_o(If1_pca4),
    .iram_ena_o(If1_iram_ena),
    .iram_addra_o(If1_iram_addra)
);

stage_if2 Stage_if2(
    .clk(clk),
    .rst(rst),
    .stall(Id_stall|iram_stall|dram_stall),
    .flush(Cp0_exc_flush),

    .pc(If1_pc),
    .pca4(If1_pca4),
    
    .iram_douta(iram_douta),

    .inst_o(If2_inst),
    .pca4_o(If2_pca4),
    .pc_o(If2_pc)
    
);


stage_id  Stage_id(
    .clk(clk),
    .rst(rst),
    .flush(Cp0_exc_flush),
    .stall(Id_stall|iram_stall|dram_stall),

    .pc(If2_pc),
    .pca4(If2_pca4),
    .inst(If2_inst),

    .ex_dstop_type(Ex_dstop_type),
    .ex_dstop_from_type(Ex_dstop_from_type),
    .ex_dstop_addr(Ex_dstop_addr),
    .ex_dstop_data(Ex_dstop_data),

    .mult_busy(Ex_md_busy),

    .mem1_dstop_type(Mem1_dstop_type),
    .mem1_dstop_from_type(Mem1_dstop_from_type),
    .mem1_dstop_addr(Mem1_dstop_addr),
    .mem1_dstop_data(Mem1_ex_mem1_dstop_data),

    .mem2_dstop_type(Mem2_dstop_type),
    .mem2_dstop_from_type(Mem2_dstop_from_type),
    .mem2_dstop_addr(Mem2_dstop_addr),
    .mem2_dstop_data(Mem2_mem1_mem2_dstop_data),

    .wb_dstop_type(Wb_dstop_type),
    .wb_dstop_from_type(Wb_dstop_from_type),
    .wb_dstop_addr(Wb_dstop_addr),
    //.wb_dstop_data(Wb_mem2_wb_dstop_data),

    .wb_dstop_data(Wb_dstop_data),

    .stall_o(Id_stall),
    .src1op_data_o(Id_src1op_data),
    .src2op_data_o(Id_src2op_data),
    
    .src2op_type_o(Id_src2op_type),
    .src1op_addr_o(Id_src1op_addr),
    .src2op_addr_o(Id_src2op_addr),

    .dstop_type_o(Id_dstop_type),
    .dstop_from_type_o(Id_dstop_from_type),
    .dstop_addr_o(Id_dstop_addr),
    .dstop_from_addr_o(Id_dstop_from_addr),
    .dstop_data_o(Id_dstop_data),

    .aluop_o(Id_aluop),
    .memop_o(Id_memop),

    .exc_en_o(Id_exc_en),
    .exc_code_o(Id_exc_code),

    .is_branch_o(Id_is_branch),
    .branch_new_pc_o(Id_branch_new_pc),
    
    .pc_o(Id_pc),
    .is_branchslot_o(Id_is_branchslot)
    

);

stage_ex Stage_ex(
    .clk(clk),
    .rst(rst),
    .flush(Cp0_exc_flush),
    .stall(iram_stall|dram_stall),

    .pc(Id_pc),
    .is_branchslot(Id_is_branchslot),

    .src1op_data(Id_src1op_data),
    .src2op_data(Id_src2op_data),
    .dstop_data(Id_dstop_data),

    .dstop_type(Id_dstop_type),
    .dstop_from_type(Id_dstop_from_type),
    .src1op_type(`OPERAND_TYPE_REG),
    .src2op_type(Id_src2op_type),
    .src1op_addr(Id_src1op_addr),
    .src2op_addr(Id_src2op_addr),

    .dstop_addr(Id_dstop_addr),
    .dstop_from_addr(Id_dstop_from_addr),

    .aluop(Id_aluop),
    .memop(Id_memop),

    .exc_en(Id_exc_en),
    .exc_code(Id_exc_code),

    .mem1_dstop_type(Mem1_dstop_type),
    .mem1_dstop_addr(Mem1_dstop_addr),
    .mem1_dstop_data(Mem1_dstop_data),

    .mem2_dstop_type(Mem2_dstop_type),
    .mem2_dstop_addr(Mem2_dstop_addr),
    .mem2_dstop_data(Mem2_dstop_data),
    .mem2_md_valid(Mem2_md_valid),
    
    .wb_dstop_type(Wb_dstop_type),
    .wb_dstop_addr(Wb_dstop_addr),
    .wb_dstop_data(Wb_dstop_data),

    .cp0_dstop_data(Cp0_dstop_data),
    
    .dstop_type_o(Ex_dstop_type),
    .dstop_from_type_o(Ex_dstop_from_type),
    .dstop_addr_o(Ex_dstop_addr),
    .dstop_from_addr_o(Ex_dstop_from_addr),
    .dstop_data_o(Ex_dstop_data),

    .memop_o(Ex_memop),
    .alu_result_o(Ex_alu_result),

    .exc_en_o(Ex_exc_en),
    .exc_code_o(Ex_exc_code),
    
    .pc_o(Ex_pc),
    .is_branchslot_o(Ex_is_branchslot),
    
.md_valid_o(Ex_md_valid),
.md_busy_o(Ex_md_busy)

);

stage_mem1 Stage_mem1(
    .clk(clk),
    .rst(rst),
    .flush(Cp0_exc_flush),
    .stall(iram_stall|dram_stall),

    .pc(Ex_pc),
    .is_branchslot(Ex_is_branchslot),

    .dstop_data(Ex_dstop_data),

    .dstop_type(Ex_dstop_type),
    .dstop_from_type(Ex_dstop_from_type),

    .dstop_addr(Ex_dstop_addr),
    .dstop_from_addr(Ex_dstop_from_addr),

    .memop(Ex_memop),
    .alu_result(Ex_alu_result),
    .md_valid(Ex_md_valid),

    .exc_en(Ex_exc_en),
    .exc_code(Ex_exc_code),

    .mem2_dstop_type(Mem2_dstop_type),
    .mem2_dstop_addr(Mem2_dstop_addr),
    .mem2_dstop_data(Mem2_dstop_data),
    
    .wb_dstop_type(Wb_dstop_type),
    .wb_dstop_addr(Wb_dstop_addr),
    .wb_dstop_data(Wb_dstop_data),

    .dstop_type_o(Mem1_dstop_type),
    .dstop_from_type_o(Mem1_dstop_from_type),
    .dstop_addr_o(Mem1_dstop_addr),
    .dstop_from_addr_o(Mem1_dstop_from_addr),
    .dstop_data_o(Mem1_dstop_data),

    .memop_o(Mem1_memop),
    .alu_result_o(Mem1_alu_result),

    .exc_en_o(Mem1_exc_en),
    .exc_code_o(Mem1_exc_code),

    .ex_mem1_dstop_data_o(Mem1_ex_mem1_dstop_data),

    .dram_ena_o(Mem1_dram_ena),
    .dram_wea_o(Mem1_dram_wea),
    .dram_addra_o(Mem1_dram_addra),
    .dram_dina_o(Mem1_dram_dina),

    .pc_o(Mem1_pc),
    .is_branchslot_o(Mem1_is_branchslot),
    
    .md_valid_o(Mem1_md_valid)
);


stage_mem2 Stage_mem2(
    .clk(clk),
    .rst(rst),
    .flush(Cp0_exc_flush),
    .stall(iram_stall|dram_stall),

    .pc(Mem1_pc),
    .is_branchslot(Mem1_is_branchslot),

    .dstop_data(Mem1_dstop_data),

    .dstop_type(Mem1_dstop_type),
    .dstop_from_type(Mem1_dstop_from_type),

    .dstop_addr(Mem1_dstop_addr),
    .dstop_from_addr(Mem1_dstop_from_addr),

    .memop(Mem1_memop),
    .alu_result(Mem1_alu_result),
    .md_valid(Mem1_md_valid),

    .exc_en(Mem1_exc_en),
    .exc_code(Mem1_exc_code),

    .dram_douta(dram_douta),

    .wb_dstop_type(Wb_dstop_type),
    .wb_dstop_addr(Wb_dstop_addr),
    .wb_dstop_data(Wb_dstop_data),

    .dstop_type_o(Mem2_dstop_type),
    .dstop_addr_o(Mem2_dstop_addr),
    .dstop_data_o(Mem2_dstop_data),
    .dstop_from_type_o(Mem2_dstop_from_type),

    .exc_en_o(Mem2_exc_en),
    .exc_code_o(Mem2_exc_code),

    .mem1_mem2_dstop_data_o(Mem2_mem1_mem2_dstop_data),

    .pc_o(Mem2_pc),
    .is_branchslot_o(Mem2_is_branchslot),
    
    .md_valid_o(Mem2_md_valid)
    
);


stage_wb Stage_wb(
    .clk(clk),
    .rst(rst),
    .flush(1'b0),
    .stall(iram_stall|dram_stall),

    .dstop_data(Mem2_dstop_data),

    .dstop_type(Mem2_dstop_type),
    .dstop_addr(Mem2_dstop_addr),
    .dstop_from_type(Mem2_dstop_from_type),
    
    .pc(Mem2_pc),
    
    .exc_en(Mem2_exc_en),
    .exc_code(Mem2_exc_code),


    .dstop_type_o(Wb_dstop_type),
    .dstop_addr_o(Wb_dstop_addr),
    .dstop_data_o(Wb_dstop_data),
    .dstop_from_type_o(Wb_dstop_from_type),

    .mem2_wb_dstop_data_o(Wb_mem2_wb_dstop_data),
    
    .debug_wb_pc_o(debug_wb_pc_o),
    .debug_wb_wen_o(debug_wb_wen_o),
    .debug_wb_wnum_o(debug_wb_wnum_o),
    .debug_wb_wdata_o(debug_wb_wdata_o)
);

cp0 Cp0(
    .clk(clk),
    .rst(rst),

    .dstop_type(Wb_dstop_type),
    .dstop_addr(Wb_dstop_addr),
    .dstop_data(Wb_dstop_data),
    
    .dstop_from_type(Ex_dstop_from_type),
    .dstop_from_addr(Ex_dstop_from_addr),
    
    .exc_en(Mem2_exc_en),
    .exc_code(Mem2_exc_code),

    .pc(Mem2_pc),
    .is_branchslot(Mem2_is_branchslot),

    .ex_int_en(int),

    .dstop_data_o(Cp0_dstop_data),

    .exc_flush_o(Cp0_exc_flush),
    
    .is_exception_o(Cp0_is_exception),
    .exception_new_pc_o(Cp0_exception_new_pc)
    
);




endmodule
