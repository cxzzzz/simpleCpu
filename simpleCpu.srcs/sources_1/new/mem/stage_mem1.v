`include "../defs.v"
module stage_mem1(
    input clk,
    input rst,
    input flush,
    input stall,

    input [31:0]dstop_data,
    input [`OPERAND_TYPE_LEN-1:0]dstop_type,dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]dstop_addr,dstop_from_addr,
    input [`MEMOP_TYPE_LEN-1:0]memop,
    input [31:0]alu_result,
    input md_valid,

    input exc_en,
    input [4:0]exc_code,

    input [31:0]pc,
    input is_branchslot,

    //from mem2
    input [`OPERAND_ADDR_LEN-1:0]mem2_dstop_addr,
    input [`OPERAND_TYPE_LEN-1:0]mem2_dstop_type,
    input [31:0]mem2_dstop_data,

    //from wb
    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]wb_dstop_addr,
    input [31:0]wb_dstop_data,

    //to mem2
    output [`OPERAND_TYPE_LEN-1:0]dstop_type_o,dstop_from_type_o,
    output [`OPERAND_ADDR_LEN-1:0]dstop_addr_o,dstop_from_addr_o,
    output [31:0]dstop_data_o,
    output [31:0]alu_result_o, 
    output md_valid_o,


    output [`MEMOP_TYPE_LEN-1:0]memop_o,

    output exc_en_o,
    output [4:0]exc_code_o,

    output [31:0]pc_o,
    output is_branchslot_o,
    //to forward
    output [31:0] ex_mem1_dstop_data_o,

    //to dram

    output dram_ena_o,
    output [3:0]dram_wea_o,
    output [31:2]dram_addra_o,
    output  [31:0]dram_dina_o
);
//ex->mem1 
reg [31:0]ex_mem1_dstop_data,ex_mem1_alu_result;
reg [`OPERAND_TYPE_LEN-1:0]ex_mem1_dstop_type,ex_mem1_dstop_from_type;
reg [`OPERAND_ADDR_LEN-1:0]ex_mem1_dstop_addr,ex_mem1_dstop_from_addr;

reg [`MEMOP_TYPE_LEN-1:0]ex_mem1_memop;

reg ex_mem1_exc_en;
reg [4:0]ex_mem1_exc_code;

reg [31:0]ex_mem1_pc;
reg ex_mem1_is_branchslot;

reg ex_mem1_md_valid;

//mem1

wire Mem1_dram_ena;
wire [3:0]Mem1_dram_wea;
wire [31:2]Mem1_dram_addra;
wire [31:0]Mem1_dram_dina;

wire [4:0]Mem1_exc_code;
wire Mem1_exc_en;

//Forward
wire [31:0]Forward_dstop_data;



assign dstop_type_o=(stall|flush)?`OPERAND_TYPE_NONE:ex_mem1_dstop_type;
assign dstop_addr_o=ex_mem1_dstop_addr;
assign dstop_from_addr_o=ex_mem1_dstop_from_addr;
assign dstop_from_type_o=(stall|flush)?`OPERAND_TYPE_NONE:ex_mem1_dstop_from_type;
assign dstop_data_o=ex_mem1_dstop_data;
assign alu_result_o=ex_mem1_alu_result;
assign md_valid_o=ex_mem1_md_valid;

assign memop_o=(stall|flush)?`MEMOP_TYPE_NONE:ex_mem1_memop;

assign exc_en_o=((stall|flush)?0:(exc_en|Mem1_exc_en));
assign exc_code_o=(exc_en?exc_code:Mem1_exc_code);

assign ex_mem1_dstop_data_o=Forward_dstop_data;


assign dram_ena_o=(stall|flush)?0:Mem1_dram_ena;
assign dram_wea_o=Mem1_dram_wea;
assign dram_addra_o=Mem1_dram_addra;
assign dram_dina_o=Mem1_dram_dina;

assign pc_o=ex_mem1_pc;
assign is_branchslot_o=(stall|flush)?0:ex_mem1_is_branchslot;


always@(posedge rst or posedge clk )
begin
    if(rst )
    begin
        ex_mem1_dstop_data<=0;
        ex_mem1_dstop_type<=0;
        ex_mem1_dstop_from_type<=0;
        ex_mem1_dstop_addr<=0;
        ex_mem1_dstop_from_addr<=0;

        ex_mem1_memop<=0;
        ex_mem1_alu_result<=0;

        ex_mem1_exc_code<=0;
        ex_mem1_exc_en<=0;

        ex_mem1_pc<=0;
        ex_mem1_is_branchslot<=0;
        ex_mem1_md_valid<=0;
    end
    else
    begin
        if(stall)
            ;
        else
        begin
            ex_mem1_dstop_data<=dstop_data;
            ex_mem1_dstop_type<=dstop_type;
            ex_mem1_dstop_from_type<=dstop_from_type;
            ex_mem1_dstop_addr<=dstop_addr;
            ex_mem1_dstop_from_addr<=dstop_from_addr;

            ex_mem1_memop<=memop;
            ex_mem1_alu_result<=alu_result;

            ex_mem1_exc_code<=exc_code;
            ex_mem1_exc_en<=exc_en;

            ex_mem1_pc<=pc;
            ex_mem1_is_branchslot<=is_branchslot;
            
            ex_mem1_md_valid<=md_valid;
        end

    end
end

mem1 Mem1(
    .addr(ex_mem1_alu_result),
    .memop(ex_mem1_memop),
    .dstop_data(ex_mem1_dstop_data),
    .dstop_type(ex_mem1_dstop_type),
    .dstop_from_type(ex_mem1_dstop_from_type),

    .dram_ena(Mem1_dram_ena),
    .dram_wea(Mem1_dram_wea),
    .dram_addra(Mem1_dram_addra),
    .dram_dina(Mem1_dram_dina),

    .exc_en(Mem1_exc_en),
    .exc_code(Mem1_exc_code)
);

forward Forward_dstop(
    .srcop_type(ex_mem1_dstop_from_type),
    .srcop_addr(ex_mem1_dstop_from_addr),
    .srcop_data_i(ex_mem1_dstop_data),

    .ex_dstop_type(`OPERAND_TYPE_NONE),
    .ex_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .ex_dstop_data(0),

    .mem1_dstop_type(`OPERAND_TYPE_NONE),
    .mem1_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .mem1_dstop_data(0),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_data(mem2_dstop_data),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_dstop_data)
);

endmodule
