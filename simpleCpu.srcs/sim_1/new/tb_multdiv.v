`timescale 1ns / 1ps

module tb_multdiv;


reg clk,rst,flush;
reg [`ALUOP_TYPE_LEN-1:0]aluop;
reg [31:0]src1op_data,src2op_data;

reg [`OPERAND_TYPE_LEN-1:0]dstop_type;
reg [31:0]dstop_data;



reg mdvalid;

reg [`OPERAND_TYPE_LEN-1:0]dstop_from_type;

wire busy_o;
wire [31:0]dstop_data_o;
wire mdvalid_o;

 multdiv Multdiv(
    .clk(clk),
    .rst(rst),
    .flush(flush),
    
    .aluop(aluop),
    .src1op_data(src1op_data),
    .src2op_data(src2op_data),
    
    .dstop_type(dstop_type),
    .dstop_data(dstop_data),
    
    .mdvalid(mdvalid),
    
    .dstop_from_type(dstop_from_type),
    .busy_o(busy_o),
    .dstop_data_o(dstop_data_o),
    .mdvalid_o(mdvalid_o)
 );
 
 always
 begin
    #5; clk=!clk;
 end
 
 integer i,valid,j;
 reg [63:0]std_result;
 reg [31:0]result_hi,result_lo;
 initial
 begin
    rst=1;
    clk=0;
    flush=0;
    #12;
    rst=0;
    
    //mfhi,mflo
    //mthi,mtlo
    for(i=0;i<100;i=i+1)
        begin
                aluop=`ALUOP_TYPE_NONE;
                dstop_data=$random;
                dstop_type=`OPERAND_TYPE_HI;
                #10;
                dstop_type=`OPERAND_TYPE_NONE;
                dstop_from_type=`OPERAND_TYPE_HI;
                #2;
                if(dstop_data_o!=dstop_data)
                begin
                $display("mthi failed");
                $stop;
                end
                
        end
     $display("mthi passed");
     
         for(i=0;i<100;i=i+1)
         begin
                 aluop=`ALUOP_TYPE_NONE;
                 dstop_data=$random;
                 dstop_type=`OPERAND_TYPE_LO;
                 #10;
                 dstop_type=`OPERAND_TYPE_NONE;
                 dstop_from_type=`OPERAND_TYPE_LO;
                 #2;
                 if(dstop_data_o!=dstop_data)
                 begin
                 $display("mthi failed");
                 $stop;
                 end
                 
         end
      $display("mtlo passed");
     
    
    //multu
    for(i=0;i<100;i=i+1)
    begin
        aluop=`ALUOP_TYPE_MULT;
        src1op_data=$random;
        src2op_data=$random;
        std_result=({32'b0,src1op_data}* {32'b0,src2op_data});

        #10;
        aluop=`ALUOP_TYPE_NONE;
        #20;
        mdvalid=1;
        #10;
        mdvalid=0;
        #70;
        
        dstop_from_type=`OPERAND_TYPE_HI;
        #10;
        result_hi=dstop_data_o;
        dstop_from_type=`OPERAND_TYPE_LO;
        #10;
        result_lo=dstop_data_o;
        if({result_hi,result_lo}!=std_result)
        begin
            $display("multu failed");
            $stop;
        end
        #20;
    end
         
     $display("multu passed");
    
    //mult
     for(i=0;i<100;i=i+1)
     begin
         aluop=`ALUOP_TYPE_MULT|`ALUOP_TYPE_SIGNED;
         src1op_data=$random;
         src2op_data=$random;
         std_result=($signed(src1op_data)* $signed(src2op_data));
 
         #10;
         aluop=`ALUOP_TYPE_NONE;
         #20;
         mdvalid=1;
         #10;
         mdvalid=0;
         #70;
         
         dstop_from_type=`OPERAND_TYPE_HI;
         #10;
         result_hi=dstop_data_o;
         dstop_from_type=`OPERAND_TYPE_LO;
         #10;
         result_lo=dstop_data_o;
         if({result_hi,result_lo}!=std_result)
         begin
             $display("mult failed");
             $stop;
         end
         #20;
     end
       $display("mult passed");
       
       rst=1;
       #20;
       rst=0;
       std_result=0;
       #20;
       
      
      //maddu
        for(i=0;i<100;i=i+1)
           begin
               aluop=`ALUOP_TYPE_MULT|`ALUOP_TYPE_MDADD;
               src1op_data=$random;
               src2op_data=$random;
               std_result={std_result}+({32'b0,src1op_data}* {32'b0,src2op_data});
       
               #10;
               aluop=`ALUOP_TYPE_NONE;
               #20;
               mdvalid=1;
               #10;
               mdvalid=0;
               #70;
               
               dstop_from_type=`OPERAND_TYPE_HI;
               #10;
               result_hi=dstop_data_o;
               dstop_from_type=`OPERAND_TYPE_LO;
               #10;
               result_lo=dstop_data_o;
               if({result_hi,result_lo}!=std_result)
               begin
                   $display("maddu failed");
                   $stop;
               end
               #20;
           end
       $display("maddu passed");
       
       
       
        //divu
          for(i=0;i<100;i=i+1)
          begin
              aluop=`ALUOP_TYPE_DIV;
              src1op_data=$random;
              src2op_data=$random;
              std_result={(src1op_data%src2op_data),(src1op_data/src2op_data)};
      
              #10;
              aluop=`ALUOP_TYPE_NONE;
              #20;
              mdvalid=1;
              #10;
              mdvalid=0;
              #300;
              
              dstop_from_type=`OPERAND_TYPE_HI;
              #10;
              result_hi=dstop_data_o;
              dstop_from_type=`OPERAND_TYPE_LO;
              #10;
              result_lo=dstop_data_o;
              if({result_hi,result_lo}!=std_result)
              begin
                  $display("divu failed");
                  $stop;
              end
              #20;
          end
               
           $display("divu passed");
           
         //invalid test
         for(i=0;i<100;i=i+1)
         begin
             aluop=`ALUOP_TYPE_MULT;
             src1op_data=$random;
             src2op_data=$random;
             //std_result=//{(src1op_data%src2op_data),(src1op_data/src2op_data)};
            
             valid=$unsigned($random)%2;
            
             #10;
             aluop=`ALUOP_TYPE_NONE;
             
             for(j=0;j<$unsigned($random)%20;j=j+1)
             begin
                #10;
             end
             if(valid==1)
             begin
                std_result=({32'b0,src1op_data}* {32'b0,src2op_data});
                mdvalid=1;
                flush=0;
                #10;
                mdvalid=0;
                #10;
             end
             else
             begin
                        mdvalid=1;
                        flush=1;
                        #10;
                        mdvalid=0;
                        flush=0;
                        #10;
             end
             
             #100;
             
             dstop_from_type=`OPERAND_TYPE_HI;
             #10;
             result_hi=dstop_data_o;
             dstop_from_type=`OPERAND_TYPE_LO;
             #10;
             result_lo=dstop_data_o;
             if({result_hi,result_lo}!=std_result)
             begin
                 $display("invalid failed");
                 $stop;
             end
             #20;
         end
         $display("invalid passed");
          
            $stop;
    
 end
 
endmodule
