module stage_id(
    input clk,
    input rst,
    input flush, //for exception
    input stall,

    //from if2
    input [31:0]pca4,
    input [31:0]inst,

    input [31:0]pc,

    //from ex
    input [`OPERAND_TYPE_LEN-1:0]ex_dstop_type,
    input [`OPERAND_TYPE_LEN-1:0]ex_dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]ex_dstop_addr,
    input [31:0]ex_dstop_data,

    input mult_busy,

    //from mem1
    input [`OPERAND_TYPE_LEN-1:0]mem1_dstop_type,
    input [`OPERAND_TYPE_LEN-1:0]mem1_dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]mem1_dstop_addr,
    input [31:0]mem1_dstop_data,

    //from mem2
    input [`OPERAND_ADDR_LEN-1:0]mem2_dstop_addr,
    input [`OPERAND_TYPE_LEN-1:0]mem2_dstop_from_type,
    input [`OPERAND_TYPE_LEN-1:0]mem2_dstop_type,
    input [31:0]mem2_dstop_data,

    //from wb
    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_type,
    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]wb_dstop_addr,
    input [31:0]wb_dstop_data,


    //to ex
    output stall_o,


    //output [`OPERAND_TYPE_LEN-1:0]src1op_type_o,
    output [`OPERAND_ADDR_LEN-1:0]src1op_addr_o,
    output [31:0]src1op_data_o,

    //output [`OPERAND_TYPE_LEN-1:0]src2op_type,
    output [`OPERAND_ADDR_LEN-1:0]src2op_addr_o,
    output [`OPERAND_TYPE_LEN-1:0]src2op_type_o,

    output [31:0]src2op_data_o,

    output [`OPERAND_TYPE_LEN-1:0]dstop_type_o,
    output [`OPERAND_TYPE_LEN-1:0]dstop_from_type_o,
    
    output [`OPERAND_ADDR_LEN-1:0]dstop_addr_o,
    output [`OPERAND_ADDR_LEN-1:0]dstop_from_addr_o,
    
    output [31:0]dstop_data_o,

    output [`ALUOP_TYPE_LEN-1:0]aluop_o,
    output [`MEMOP_TYPE_LEN-1:0]memop_o,

    output exc_en_o,
    output [4:0]exc_code_o,

    output [31:0]pc_o,

    //to if1
    output is_branch_o,
    output [31:0]branch_new_pc_o ,
    output is_branchslot_o

);

//if2->id
reg [31:0]if2_id_pca4,if2_id_inst,if2_id_pc;

//Ext
wire [31:0]Ext_sign_ext,Ext_unsign_ext,Ext_shift_ext;

//Id

wire [`OPERAND_ADDR_LEN-1:0]Id_src1op_addr,Id_src2op_addr;
wire [`OPERAND_TYPE_LEN-1:0]Id_src2op_type,Id_dstop_type,Id_dstop_from_type;
wire [`OPERAND_ADDR_LEN-1:0]Id_dstop_addr,Id_dstop_from_addr;
wire [`T_LEN-1:0]Id_src1op_tuse,Id_src2op_tuse,Id_dstop_from_tuse;
wire [`ALUOP_TYPE_LEN-1:0]Id_aluop;
wire [`MEMOP_TYPE_LEN-1:0]Id_memop;
wire [`JADDR_TYPE_LEN-1:0]Id_jaddr_type;

wire [4:0]Id_exc_code;
wire Id_exc_en;

wire grf_rd_wren=(wb_dstop_type==`OPERAND_TYPE_REG);

//Branch
wire [31:0]Branch_branch_new_pc;
wire Branch_is_branch;

//Freeze
wire Freeze_stall;// src1op_stall,src2op_stall,dstop_stall;

//Forward
wire [31:0]Forward_src1op_data,Forward_src2op_data,Forward_dstop_data;

//mux
wire [31:0]Mux_src2op_data;
wire [31:0]Mux_dstop_data;


//grf
wire [31:0]Grf_rs_data;
wire [31:0]Grf_rt_data;

//is_branchslot
reg next_is_branchslot;
always@(posedge rst or posedge clk)
begin
    if(rst)
        next_is_branchslot<=0;
    else
        next_is_branchslot<=(Id_jaddr_type!=`JADDR_TYPE_NONE);
end


assign stall_o=Freeze_stall;
assign src1op_data_o=Forward_src1op_data;
assign src2op_data_o=Mux_src2op_data;
assign src2op_type_o=Id_src2op_type;
assign src1op_addr_o=Id_src1op_addr;
assign src2op_addr_o=Id_src2op_addr;

assign dstop_type_o=(stall|flush)?`OPERAND_TYPE_NONE:Id_dstop_type;
assign dstop_from_type_o=(stall|flush)?`OPERAND_TYPE_NONE:Id_dstop_from_type;
assign dstop_addr_o=Id_dstop_addr;
assign dstop_from_addr_o=Id_dstop_from_addr;
assign dstop_data_o=Mux_dstop_data;

assign aluop_o=(stall|flush)?`ALUOP_TYPE_NONE:Id_aluop;
assign memop_o=(stall|flush)?`MEMOP_TYPE_NONE:Id_memop;

assign exc_en_o=(stall|flush)?0:Id_exc_en;
assign exc_code_o=Id_exc_code;

assign is_branch_o=(stall|flush)?0:Branch_is_branch;
assign branch_new_pc_o=Branch_branch_new_pc;

assign pc_o=if2_id_pc;
assign is_branchslot_o=(stall|flush)?0:next_is_branchslot;


//id->ex

always@(posedge clk or posedge rst )
begin
    if(rst )
    begin
        if2_id_pca4<=0;
        if2_id_inst<=0;
        if2_id_pc<=0;
    end
    else //clk
    begin
        if(stall);
        else 
        begin
            if2_id_pc<=pc;
            if2_id_pca4<=pca4;
            if2_id_inst<=inst;
        end
    end
end

id Id(
    .inst(if2_id_inst),

    //output
    .src1op_addr(Id_src1op_addr),
    .src1op_tuse(Id_src1op_tuse),

    .src2op_type(Id_src2op_type),
    .src2op_addr(Id_src2op_addr),
    .src2op_tuse(Id_src2op_tuse),

    .dstop_type(Id_dstop_type),
    .dstop_from_type(Id_dstop_from_type),

    .dstop_addr(Id_dstop_addr),
    .dstop_from_addr(Id_dstop_from_addr),
    .dstop_from_tuse(Id_dstop_from_tuse),

    .aluop(Id_aluop),
    .memop(Id_memop),
    .jaddr_type(Id_jaddr_type),
    .exc_code(Id_exc_code),
    .exc_en(Id_exc_en)
);

grf Grf(
    .clk(clk),
    .rst(rst),
    .rs_addr(Id_src1op_addr[4:0]),
    .rt_addr((Id_src2op_type==`OPERAND_TYPE_REG)?Id_src2op_addr[4:0]:Id_dstop_from_addr[4:0]),

    .rd_addr(wb_dstop_addr[4:0]),
    .rd_data(wb_dstop_data),
    .rd_wren(grf_rd_wren),

    .rs_data(Grf_rs_data),
    .rt_data(Grf_rt_data)
);


branch Branch(
    .pca4(if2_id_pca4),
    .inst(if2_id_inst),
    .jaddr_type(Id_jaddr_type),
    .aluop(Id_aluop),
    .src1op_data(Forward_src1op_data),
    .src2op_data(Mux_src2op_data),

    //output
    .branch_new_pc(Branch_branch_new_pc),
    .is_branch(Branch_is_branch)
);

freeze Freeze(
    .src1op_addr(Id_src1op_addr),
    .src1op_tuse(Id_src1op_tuse), 

    .src2op_type(Id_src2op_type),
    .src2op_addr(Id_src2op_addr),
    .src2op_tuse(Id_src2op_tuse), 

    .dstop_type(Id_dstop_type),
    .dstop_addr(Id_dstop_addr),

    .dstop_from_type(Id_dstop_from_type),
    .dstop_from_addr(Id_dstop_from_addr),
    .dstop_from_tuse(Id_dstop_from_tuse),

    .ex_dstop_type(ex_dstop_type),
    .ex_dstop_addr(ex_dstop_addr),
    .ex_dstop_from_type(ex_dstop_from_type),

    .mem1_dstop_type(mem1_dstop_type),
    .mem1_dstop_addr(mem1_dstop_addr),
    .mem1_dstop_from_type(mem1_dstop_from_type),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_from_type(mem2_dstop_from_type),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_from_type(wb_dstop_from_type),

    .aluop(Id_aluop),
    .mult_busy(mult_busy),
    .stall(Freeze_stall)
);

ext Ext(
    .inst(if2_id_inst),
    .sign_ext(Ext_sign_ext),
    .unsign_ext(Ext_unsign_ext),
    .shift_ext(Ext_shift_ext)
);

mux4 
#(  .TYPE_LEN(`OPERAND_TYPE_LEN),
    .OP1_TYPE(`OPERAND_TYPE_IMME),
    .OP2_TYPE(`OPERAND_TYPE_UIMME),
    .OP3_TYPE(`OPERAND_TYPE_SHIFT)
)

Mux_src2opdata
(
    .op_type(Id_src2op_type),
    .op1_data(Ext_sign_ext),
    .op2_data(Ext_unsign_ext),
    .op3_data(Ext_shift_ext),
    .op4_data(Forward_src2op_data),  //grf
    .op_data(Mux_src2op_data)
); 

mux2
#(
    .TYPE_LEN(`OPERAND_TYPE_LEN),
    .OP1_TYPE(`OPERAND_TYPE_PC)
)
Mux_dstopdata
(
    .op_type(Id_dstop_from_type),
    .op1_data(if2_id_pca4 + 4),
    .op2_data(Forward_dstop_data),
    .op_data(Mux_dstop_data)
);
 

forward Forward_src1op(
    .srcop_type(`OPERAND_TYPE_REG),
    .srcop_addr(Id_src1op_addr),
    .srcop_data_i(Grf_rs_data),

    .ex_dstop_type(ex_dstop_type),
    .ex_dstop_addr(ex_dstop_addr),
    .ex_dstop_data(ex_dstop_data),

    .mem1_dstop_type(mem1_dstop_type),
    .mem1_dstop_addr(mem1_dstop_addr),
    .mem1_dstop_data(mem1_dstop_data),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_data(mem2_dstop_data),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_src1op_data)
);

forward Forward_src2op(
    .srcop_type(Id_src2op_type),
    .srcop_addr(Id_src2op_addr),
    .srcop_data_i(Grf_rt_data),

    .ex_dstop_type(ex_dstop_type),
    .ex_dstop_addr(ex_dstop_addr),
    .ex_dstop_data(ex_dstop_data),

    .mem1_dstop_type(mem1_dstop_type),
    .mem1_dstop_addr(mem1_dstop_addr),
    .mem1_dstop_data(mem1_dstop_data),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_data(mem2_dstop_data),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_src2op_data)
);

forward Forward_dstop(
    .srcop_type(Id_dstop_from_type),
    .srcop_addr(Id_dstop_from_addr),
    .srcop_data_i(Grf_rt_data),

    .ex_dstop_type(ex_dstop_type),
    .ex_dstop_addr(ex_dstop_addr),
    .ex_dstop_data(ex_dstop_data),

    .mem1_dstop_type(mem1_dstop_type),
    .mem1_dstop_addr(mem1_dstop_addr),
    .mem1_dstop_data(mem1_dstop_data),

    .mem2_dstop_type(mem2_dstop_type),
    .mem2_dstop_addr(mem2_dstop_addr),
    .mem2_dstop_data(mem2_dstop_data),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_dstop_data)
);

endmodule
