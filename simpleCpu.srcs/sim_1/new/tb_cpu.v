`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/24/2018 04:17:39 PM
// Design Name: 
// Module Name: tb_cpu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_cpu;

    reg clk,rst;
    
    //iram
    reg [31:0]iram_douta;
    reg iram_stall;
    
    wire [31:2]iram_addra_o;
    wire iram_ena_o;
    
    //dram
    reg [31:0]dram_douta;
    reg dram_stall;
    
    wire dram_ena_o;
    wire [31:2]dram_addra_o;
    wire [3:0]dram_wea_o;
    wire [31:0]dram_dina_o;
    
    //int
    reg [5:0]int;
    
    
cpu_top Cpu(
    .clk(clk),
    .rst(rst),
    
    .iram_ena_o(iram_ena_o),
    .iram_addra_o(iram_addra_o),
    .iram_douta(iram_douta),
    .iram_stall(iram_stall),
    
    .dram_ena_o(dram_ena_o),
    .dram_addra_o(dram_addra_o),
    .dram_wea_o(dram_wea_o),
    .dram_dina_o(dram_dina_o),
    .dram_douta(dram_douta),
    .dram_stall(dram_stall),
    
    .int(int)
);
 
 reg [31:0]iram[255:0]; 
 
 always
 begin
    #5;
    clk=~clk;
 end
 
 always@(posedge clk)
 begin
    iram_douta=iram[iram_addra_o[9:2]];
 end
 
integer i;
initial
begin
    rst=1;
    clk=0;
    iram_stall=0;
    dram_stall=0;
    int=0;
    
    for(i=0;i<256;i=i+1)
        iram[i]=0;
      
    iram[0]=32'h3c020100;//lui
    iram[1]=32'h34420200;//ori
    iram[2]=32'h00420821;//add
    iram[3]=32'h1063fffc;//beq
    
    dram_douta=32'b0;
    
    #13;
    
    
    rst=0;
    
    #200;
    $stop;
end
    
endmodule
