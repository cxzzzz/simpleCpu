`include "../defs.v"

module stage_mem2(
    input clk,
    input rst,
    input flush,
    input stall,

    //from mem1
    input [31:0]dstop_data,alu_result,
    input [`OPERAND_TYPE_LEN-1:0]dstop_type,dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]dstop_addr,dstop_from_addr,
    input [`MEMOP_TYPE_LEN-1:0]memop,
    input [31:0]pc,
    input is_branchslot,
    
    input md_valid,

    input exc_en,
    input [4:0]exc_code,



    //from wb
    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]wb_dstop_addr,
    input [31:0]wb_dstop_data,

    //from dram
    input [31:0]dram_douta,


    //to wb
    output [`OPERAND_TYPE_LEN-1:0]dstop_type_o,dstop_from_type_o,
    output [`OPERAND_ADDR_LEN-1:0]dstop_addr_o,
    output [31:0]dstop_data_o,
    

    output exc_en_o,
    output [4:0]exc_code_o,
    output [31:0]pc_o,

    output is_branchslot_o,
    //to forward
    output [31:0]mem1_mem2_dstop_data_o,
    
    //to ex
    output md_valid_o


    );

//mem1->mem2
reg [31:0]mem1_mem2_dstop_data,mem1_mem2_alu_result;
reg [`OPERAND_TYPE_LEN-1:0]mem1_mem2_dstop_type,mem1_mem2_dstop_from_type;
reg [`OPERAND_ADDR_LEN-1:0]mem1_mem2_dstop_addr,mem1_mem2_dstop_from_addr;

reg [`MEMOP_TYPE_LEN-1:0]mem1_mem2_memop;

reg mem1_mem2_exc_en;
reg [4:0]mem1_mem2_exc_code;

reg [31:0]mem1_mem2_pc;
reg [31:0]mem1_mem2_is_branchslot;

reg mem1_mem2_md_valid;


//mem2
wire [31:0]Mem2_dstop_data;

//mux
wire [31:0]Mux_dstop_data;


//forward
wire [31:0]Forward_dstop_data;



assign dstop_type_o=(stall|flush)?`OPERAND_TYPE_NONE:mem1_mem2_dstop_type;
assign dstop_from_type_o=(stall|flush)?`OPERAND_TYPE_NONE:mem1_mem2_dstop_from_type;
assign dstop_addr_o=mem1_mem2_dstop_addr;
assign dstop_data_o=Mux_dstop_data;

assign mem1_mem2_dstop_data_o=mem1_mem2_dstop_data;

assign exc_en_o=(stall)?0:mem1_mem2_exc_en;
assign exc_code_o=mem1_mem2_exc_code;

assign pc_o=mem1_mem2_pc;
assign is_branchslot_o=(stall|flush)?0:mem1_mem2_is_branchslot;

assign md_valid_o=mem1_mem2_md_valid;

always@(posedge clk or posedge rst )
begin
    if(rst)
    begin
        mem1_mem2_dstop_data<=0;
        mem1_mem2_dstop_type<=0;
        mem1_mem2_dstop_from_type<=0;
        mem1_mem2_dstop_addr<=0;
        mem1_mem2_dstop_from_addr<=0;
        
        mem1_mem2_memop<=0;
        mem1_mem2_alu_result<=0;

        mem1_mem2_exc_code<=0;
        mem1_mem2_exc_en<=0;

        mem1_mem2_pc<=0;
        mem1_mem2_is_branchslot<=0;
        
        mem1_mem2_md_valid<=0;
    end
    else
    begin
        if(stall);
        else
        begin
            mem1_mem2_dstop_data<=dstop_data;
            mem1_mem2_dstop_type<=dstop_type;
            mem1_mem2_dstop_from_type<=dstop_from_type;
            mem1_mem2_dstop_addr<=dstop_addr;
            mem1_mem2_dstop_from_addr<=dstop_from_addr;

            mem1_mem2_memop<=memop;
            mem1_mem2_alu_result<=alu_result;

            mem1_mem2_exc_code<=exc_code;
            mem1_mem2_exc_en<=exc_en;

            mem1_mem2_pc<=pc;
            mem1_mem2_is_branchslot<=is_branchslot;
            
            mem1_mem2_md_valid<=md_valid;
        end

    end
end

mem2 Mem2(
    .dram_douta(dram_douta),
    .addr(mem1_mem2_alu_result),
    .memop(mem1_mem2_memop),
    .dstop_data(Mem2_dstop_data)
);


mux2 
#(  .TYPE_LEN(`OPERAND_TYPE_LEN),
    .OP1_TYPE(`OPERAND_TYPE_MEM)
)
Mux
(
    .op_type(mem1_mem2_dstop_from_type),
    .op1_data(Mem2_dstop_data),
    .op2_data(Forward_dstop_data),

    .op_data(Mux_dstop_data)
);

forward Forward_dstop(
    .srcop_type(mem1_mem2_dstop_from_type),
    .srcop_addr(mem1_mem2_dstop_from_addr),
    .srcop_data_i(mem1_mem2_dstop_data),

    .ex_dstop_type(`OPERAND_TYPE_NONE),
    .ex_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .ex_dstop_data(0),

    .mem1_dstop_type(`OPERAND_TYPE_NONE),
    .mem1_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .mem1_dstop_data(0),

    .mem2_dstop_type(`OPERAND_TYPE_NONE),
    .mem2_dstop_addr(`OPERAND_ADDR_LEN'b0),
    .mem2_dstop_data(0),

    .wb_dstop_type(wb_dstop_type),
    .wb_dstop_addr(wb_dstop_addr),
    .wb_dstop_data(wb_dstop_data),

    .srcop_data_o(Forward_dstop_data)
);
endmodule
