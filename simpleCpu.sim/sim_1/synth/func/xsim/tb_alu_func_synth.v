// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Mon Jul 16 10:35:59 2018
// Host        : eda-srv running 64-bit Red Hat Enterprise Linux Server release 6.6 (Santiago)
// Command     : write_verilog -mode funcsim -nolib -force -file
//               /home/xingzhou/programming/cpu/simpleCpu/simpleCpu.sim/sim_1/synth/func/xsim/tb_alu_func_synth.v
// Design      : cpu_top
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg676-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* NotValidForBitStream *)
module cpu_top
   (sel2,
    sel1,
    sel0,
    a_assign,
    a_case,
    a_if,
    a_caseq);
  input sel2;
  input sel1;
  input sel0;
  output [0:1]a_assign;
  output [0:1]a_case;
  output [0:1]a_if;
  output [0:1]a_caseq;

  wire [0:1]a_assign;
  wire [0:0]a_assign_OBUF;
  wire [0:1]a_case;
  wire [0:1]a_case_OBUF;
  wire [0:1]a_caseq;
  wire [0:1]a_caseq_OBUF;
  wire [0:1]a_if;
  wire [0:1]a_if_OBUF;
  wire sel0;
  wire sel1;
  wire sel2;
  wire sel2_IBUF;

  OBUF \a_assign_OBUF[0]_inst 
       (.I(a_assign_OBUF),
        .O(a_assign[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \a_assign_OBUF[0]_inst_i_1 
       (.I0(a_case_OBUF[0]),
        .I1(a_case_OBUF[1]),
        .O(a_assign_OBUF));
  OBUF \a_assign_OBUF[1]_inst 
       (.I(a_if_OBUF[1]),
        .O(a_assign[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \a_assign_OBUF[1]_inst_i_1 
       (.I0(a_case_OBUF[1]),
        .I1(a_case_OBUF[0]),
        .O(a_if_OBUF[1]));
  OBUF \a_case_OBUF[0]_inst 
       (.I(a_case_OBUF[0]),
        .O(a_case[0]));
  OBUF \a_case_OBUF[1]_inst 
       (.I(a_case_OBUF[1]),
        .O(a_case[1]));
  OBUF \a_caseq_OBUF[0]_inst 
       (.I(a_caseq_OBUF[0]),
        .O(a_caseq[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \a_caseq_OBUF[0]_inst_i_1 
       (.I0(a_case_OBUF[1]),
        .I1(a_case_OBUF[0]),
        .O(a_caseq_OBUF[0]));
  OBUF \a_caseq_OBUF[1]_inst 
       (.I(a_caseq_OBUF[1]),
        .O(a_caseq[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \a_caseq_OBUF[1]_inst_i_1 
       (.I0(a_case_OBUF[0]),
        .I1(sel2_IBUF),
        .I2(a_case_OBUF[1]),
        .O(a_caseq_OBUF[1]));
  OBUF \a_if_OBUF[0]_inst 
       (.I(a_if_OBUF[0]),
        .O(a_if[0]));
  LUT2 #(
    .INIT(4'h2)) 
    \a_if_OBUF[0]_inst_i_1 
       (.I0(a_case_OBUF[0]),
        .I1(a_case_OBUF[1]),
        .O(a_if_OBUF[0]));
  OBUF \a_if_OBUF[1]_inst 
       (.I(a_if_OBUF[1]),
        .O(a_if[1]));
  IBUF sel0_IBUF_inst
       (.I(sel0),
        .O(a_case_OBUF[0]));
  IBUF sel1_IBUF_inst
       (.I(sel1),
        .O(a_case_OBUF[1]));
  IBUF sel2_IBUF_inst
       (.I(sel2),
        .O(sel2_IBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
