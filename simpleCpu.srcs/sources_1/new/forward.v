`include "defs.v"

//only forward reg
module forward
(

    input [`OPERAND_TYPE_LEN-1:0]srcop_type,
    input [`OPERAND_ADDR_LEN-1:0]srcop_addr,
    //input [`T_LEN-1:0]srcop_tuse,
    input [31:0]srcop_data_i,
    
    input [`OPERAND_TYPE_LEN-1:0]ex_dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]ex_dstop_addr,
    input [31:0]ex_dstop_data,
    //input [`T_LEN-1:0]ex_dstop_tnew,

    input [`OPERAND_TYPE_LEN-1:0]mem1_dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]mem1_dstop_addr,
    input [31:0]mem1_dstop_data,
    //input [`T_LEN-1:0]mem1_dstop_tnew,

    input [`OPERAND_ADDR_LEN-1:0]mem2_dstop_addr,
    input [`OPERAND_TYPE_LEN-1:0]mem2_dstop_type,
    input [31:0]mem2_dstop_data,
    //input [`T_LEN-1:0]mem2_dstop_tnew,

    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]wb_dstop_addr,
    input [31:0]wb_dstop_data,
    //input [`T_LEN-1:0]wb_dstop_tnew,

    output reg [31:0]srcop_data_o
);
    
    wire[`OPERAND_TYPE_LEN+`OPERAND_ADDR_LEN-1:0]srcop_id,ex_dstop_id,mem1_dstop_id,mem2_dstop_id,wb_dstop_id;

    assign srcop_id={srcop_type,srcop_addr};
    assign ex_dstop_id={ex_dstop_type,ex_dstop_addr};
    assign mem1_dstop_id={mem1_dstop_type,mem1_dstop_addr};
    assign mem2_dstop_id={mem2_dstop_type,mem2_dstop_addr};
    assign wb_dstop_id={wb_dstop_type,wb_dstop_addr};

    always@(*)
    begin
        if(srcop_type!=`OPERAND_TYPE_REG || srcop_addr==`OPERAND_ADDR_LEN'b0)
            srcop_data_o=srcop_data_i;
       /* if(!(srcop_type==`OPERAND_TYPE_REG ||
             srcop_type==`OPERAND_TYPE_HI  ||
             srcop_type==`OPERAND_TYPE_LO)
            || srcop_id=={`OPERAND_TYPE_REG,`OPERAND_ADDR_LEN'b0})
             srcop_data_o=srcop_data_i;*/
        else if(srcop_id==ex_dstop_id)
        begin
            srcop_data_o=ex_dstop_data;
        end
        else if(srcop_id==mem1_dstop_id )
        begin
            srcop_data_o=mem1_dstop_data;
        end
        else if(srcop_id==mem2_dstop_id )
        begin
            srcop_data_o=mem2_dstop_data;
        end
        else if(srcop_id==wb_dstop_id )
        begin
            srcop_data_o=wb_dstop_data;
        end
        else
            srcop_data_o=srcop_data_i;
    end
endmodule

module freeze(
    input [`OPERAND_ADDR_LEN-1:0]src1op_addr,
    input [`T_LEN-1:0]src1op_tuse,

    input [`OPERAND_TYPE_LEN-1:0]src2op_type,
    input [`OPERAND_ADDR_LEN-1:0]src2op_addr,
    input [`T_LEN-1:0]src2op_tuse,

    input [`OPERAND_TYPE_LEN-1:0]dstop_type,
    input [`OPERAND_ADDR_LEN-1:0]dstop_addr,
    input [`T_LEN-1:0]dstop_from_tuse,


    input [`OPERAND_TYPE_LEN-1:0]dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]dstop_from_addr,
    
    input [`OPERAND_TYPE_LEN-1:0]ex_dstop_type,
    input [`OPERAND_TYPE_LEN-1:0]ex_dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]ex_dstop_addr,

    input [`OPERAND_TYPE_LEN-1:0]mem1_dstop_type,
    input [`OPERAND_TYPE_LEN-1:0]mem1_dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]mem1_dstop_addr,

    input [`OPERAND_ADDR_LEN-1:0]mem2_dstop_addr,
    input [`OPERAND_TYPE_LEN-1:0]mem2_dstop_from_type,
    input [`OPERAND_TYPE_LEN-1:0]mem2_dstop_type,

    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_type,
    input [`OPERAND_TYPE_LEN-1:0]wb_dstop_from_type,
    input [`OPERAND_ADDR_LEN-1:0]wb_dstop_addr,

    input [`ALUOP_TYPE_LEN-1:0]aluop,
    input mult_busy,

    output stall
);

    //reg:与reg转发相关的stall
    function automatic [`T_LEN-1:0]type_to_tnew;
        input [`OPERAND_TYPE_LEN-1:0]dstop_from_type;
        begin
            case(dstop_from_type)
                `OPERAND_TYPE_ALU,
                `OPERAND_TYPE_HI,
                `OPERAND_TYPE_LO,
                `OPERAND_TYPE_CP0:
                    type_to_tnew=2;
                `OPERAND_TYPE_MEM: //can't forward
                    type_to_tnew=4;
                default://from reg
                    type_to_tnew=7;
            endcase
        end
    endfunction

    function automatic is_stall;
        input [`T_LEN-1:0]srcop_tuse,dstop_tnew,dt;
        input [`OPERAND_TYPE_LEN+`OPERAND_ADDR_LEN-1:0]srcop_id,dstop_id;
        begin
            if(srcop_id[`OPERAND_ADDR_LEN-1:0]==0 || srcop_id[`OPERAND_TYPE_LEN+`OPERAND_ADDR_LEN-1:`OPERAND_ADDR_LEN]!=`OPERAND_TYPE_REG)
                is_stall=0;
            else if(srcop_id==dstop_id && {1'b0,srcop_tuse}+{1'b0,dt}<{1'b0,dstop_tnew})
                is_stall=1;
            else 
                is_stall=0;
        end
    endfunction


    wire mfhl_stall,mult_stall,cp0_stall,reg_stall;
    //wire src1op_stall,src2op_stall;

    wire [`OPERAND_TYPE_LEN+`OPERAND_ADDR_LEN-1:0] src1op_id,src2op_id,ex_dstop_id,mem1_dstop_id,mem2_dstop_id,wb_dstop_id,dstop_from_id;
    wire [`T_LEN-1:0]ex_dstop_tnew,mem1_dstop_tnew,mem2_dstop_tnew,wb_dstop_tnew;
    assign src1op_id={`OPERAND_TYPE_REG,src1op_addr};
    assign src2op_id={src2op_type,src2op_addr};
    assign dstop_from_id={dstop_from_type,dstop_from_addr};
    assign ex_dstop_id={ex_dstop_type,ex_dstop_addr};
    assign mem1_dstop_id={mem1_dstop_type,mem1_dstop_addr};
    assign mem2_dstop_id={mem2_dstop_type,mem2_dstop_addr};
    assign wb_dstop_id={wb_dstop_type,wb_dstop_addr};

    assign  ex_dstop_tnew=type_to_tnew(ex_dstop_from_type);
    assign  mem1_dstop_tnew=type_to_tnew(mem1_dstop_from_type);
    assign  mem2_dstop_tnew=type_to_tnew(mem2_dstop_from_type);
    assign  wb_dstop_tnew=type_to_tnew(wb_dstop_from_type);

    //cp0:与cp0相关的stall
    assign cp0_stall=(dstop_from_type==`OPERAND_TYPE_CP0)
                    &&(
                        (dstop_from_id==ex_dstop_id)||
                        (dstop_from_id==mem1_dstop_id)||
                        (dstop_from_id==mem2_dstop_id)||
                        (dstop_from_id==wb_dstop_id)
                    );

    //mult:与mult相关的stall
    assign mult_stall=mult_busy && 
        (   (dstop_from_type==`OPERAND_TYPE_HI 
            || dstop_from_type==`OPERAND_TYPE_LO
            ||dstop_type==`OPERAND_TYPE_HI 
            || dstop_type==`OPERAND_TYPE_LO) 
            || (aluop&`ALUOP_TYPE_MULT !=0 ) 
            || (aluop&`ALUOP_TYPE_DIV !=0)
        );
        
    //mfhi,mflo:与mfhi,mflo相关的stall
    assign mfhl_stall=(dstop_from_type==`OPERAND_TYPE_HI || dstop_from_type==`OPERAND_TYPE_LO)
                        &&(
                            (dstop_from_id==ex_dstop_id)||
                            (dstop_from_id==mem1_dstop_id)||
                            (dstop_from_id==mem2_dstop_id)||
                            (dstop_from_id==wb_dstop_id)
                        );
    

    assign  ex_dstop_tnew=type_to_tnew(ex_dstop_from_type);
    assign  mem1_dstop_tnew=type_to_tnew(mem1_dstop_from_type);
    assign  mem2_dstop_tnew=type_to_tnew(mem2_dstop_from_type);
    assign  wb_dstop_tnew=type_to_tnew(wb_dstop_from_type);

    assign reg_stall=
        is_stall(src1op_tuse,ex_dstop_tnew,1,src1op_id,ex_dstop_id)|
        is_stall(src1op_tuse,mem1_dstop_tnew,2,src1op_id,mem1_dstop_id)|
        is_stall(src1op_tuse,mem2_dstop_tnew,3,src1op_id,mem2_dstop_id)|
        is_stall(src1op_tuse,wb_dstop_tnew,4,src1op_id,wb_dstop_id)|
        
        is_stall(src2op_tuse,ex_dstop_tnew,1,src2op_id,ex_dstop_id)|
        is_stall(src2op_tuse,mem1_dstop_tnew,2,src2op_id,mem1_dstop_id)|
        is_stall(src2op_tuse,mem2_dstop_tnew,3,src2op_id,mem2_dstop_id)|
        is_stall(src2op_tuse,wb_dstop_tnew,4,src2op_id,wb_dstop_id)|
        
        is_stall(dstop_from_tuse,ex_dstop_tnew,1,dstop_from_id,ex_dstop_id)|
        is_stall(dstop_from_tuse,mem1_dstop_tnew,2,dstop_from_id,mem1_dstop_id)|
        is_stall(dstop_from_tuse,mem2_dstop_tnew,3,dstop_from_id,mem2_dstop_id)|
        is_stall(dstop_from_tuse,wb_dstop_tnew,4,dstop_from_id,wb_dstop_id);
    
    assign stall=mfhl_stall|mult_stall|reg_stall|cp0_stall;

endmodule

