`include "defs.v"

module cp0(
    input clk,
    input rst,



    //mfc0 mtc0
    input [`OPERAND_TYPE_LEN-1:0]dstop_type,dstop_from_type,
    
    input [`OPERAND_ADDR_LEN-1:0]dstop_addr,dstop_from_addr,

    input [31:0]dstop_data,
    
    //exception
    input exc_en,
    input [4:0]exc_code,

    input [31:0]pc,
    input is_branchslot,
    
    input badvaddr,

    //interrupt
    input [5:0]ex_int_en,


    output reg [31:0]dstop_data_o,

    output exc_flush_o,
    output is_exception_o,
    output [31:0]exception_new_pc_o

);

wire cp0_wren=(dstop_type==`OPERAND_TYPE_CP0);

//PRId
wire [31:0]PRId={16'b0,8'd1,8'hff};

//SR
reg [15:8]SR_IM;    //bit 15-8
reg SR_EXL;  //bit 1
reg SR_IE;  //bit 0

wire [31:0]SR={9'b0,1'b1,6'b0,SR_IM,6'b0,SR_EXL,SR_IE};
//Cause
reg Cause_BD;   //bit 31
reg Cause_TI;   //bit 30
//reg Cause_CE;   //bit 29-28
reg [15:10]Cause_IPH;
reg [9:8]Cause_IPL;  //only these bits can be writen

reg [6:2]Cause_Exccode;  //bit 6-2

wire [31:0]Cause={Cause_BD,1'b1,14'b0,Cause_IPH,Cause_IPL,1'b0,Cause_Exccode,2'b0};
//EPC
reg [31:0]EPC;  //rw

//Badaddr
reg [31:0]Badaddr; //read only


//exception:
wire [7:6]in_int_en=2'b0;//null

wire [7:0]int_en={(in_int_en),(ex_int_en)}|SR_IM;

wire exception=((|int_en) & (!SR_EXL) & (SR_IE)) //interrupt
                | (exc_en);   //exception

assign exc_flush_o=exception;
assign is_exception_o=exception;
assign exception_new_pc_o=(exc_code==`EXCEPTION_TYPE_ERET)?EPC:`EXCEPTION_ADDR;


always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        SR_IM=0;
        SR_EXL=0;
        SR_IE=0;

        Cause_BD=0;
        //Cause_CE=0;
        Cause_Exccode=0;
        Cause_IPH=0;
        Cause_IPL=0;

        EPC=0;

        Badaddr=0;
    end
    else  //if a reg is written by exception and mtc0 at the same time ,exception is 
    begin
        if(cp0_wren)
        begin
            case(dstop_addr)
                `CP0_SR_ADDR:
                begin
                    SR_IM=dstop_data[15:0];
                    SR_EXL=dstop_data[1];
                    SR_IE=dstop_data[0];
                end 
                `CP0_CAUSE_ADDR:
                begin
                    //Cause_BD=dstop_data[31]; read only
                    //Cause_CE=dstop_data[29:28];
                    //Cause_IP=dstop_data[15:8]; 
                    Cause_IPL=dstop_data[9:8];
                    //Cause_Exccode=dstop_data[6:2]; //read only
                end     
                `CP0_EPC_ADDR:
                    EPC=dstop_data;
                `CP0_BADVADDR_ADDR:
                    ;//Badaddr=dstop_data;　　//read only
            endcase
        end
        if(exception)
            begin
                if(exc_code==`EXCEPTION_TYPE_ERET) //eret
                begin
                    SR_EXL=0;
                end
                else
                begin       //exception
                    EPC=is_branchslot?pc-4:pc;
                    Cause_BD=is_branchslot;
                    Cause_Exccode=exc_en?exc_code:`EXCEPTION_TYPE_INT;
                    SR_EXL=1;
                end
            end
    end
end

always@(*)
begin
    case (dstop_from_addr)
        `CP0_PRID_ADDR:
            dstop_data_o=PRId;
        `CP0_SR_ADDR:
            dstop_data_o=SR;
        `CP0_CAUSE_ADDR:
            dstop_data_o=Cause;
        `CP0_EPC_ADDR:
            dstop_data_o=EPC;
        `CP0_BADVADDR_ADDR:
            dstop_data_o=Badaddr;
        default:
            dstop_data_o=0;
    endcase
end

endmodule
