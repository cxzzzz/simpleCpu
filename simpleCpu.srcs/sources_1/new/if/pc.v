`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/13/2018 09:39:31 AM
// Design Name: 
// Module Name: pc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pc
//parameter
#(
    parameter INITIAL_PC = 32'h00000000
)
(

    //input
    input clk,
    input rst,
    input stall,

    input is_branch,
    input is_exception,
    input [31:0] branch_new_pc,
    input [31:0] exception_new_pc,
    

    //output
    output reg[31:0] pc,
    output [31:0] pca4
    );

    assign pca4 = pc+32'h4;
    reg [31:0] pc_new,pc_old;

always@(posedge rst or posedge clk)
    begin
        if(rst)
        begin
            pc_new<=INITIAL_PC;
            pc_old<=0;
        end
        else
        if(stall);
        else
        begin
            pc_new<=pca4;
            pc_old<=pc;
        end
    end
    
       always@(*)
        begin
            if(stall)
                pc=pc_old;
            else if(is_exception)
                pc=exception_new_pc;
            else if(is_branch)
                pc=branch_new_pc;
            else //normal
                pc=pc_new;
        end


endmodule
