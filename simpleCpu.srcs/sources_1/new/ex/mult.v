`include "../defs.v"

`define MULT_CLOCK 6


/*module _mult(
    //input
    input clk,
    input rst,
    input [`ALUOP_TYPE_LEN-1:0]aluop,

    input [31:0]rs_data,
    input [31:0]rt_data,

    input wr_en,
    input addr, //0:lo 1:hi
    input [31:0]wdata,

    //output
    output busy,
    output [31:0]data
    );
   
    
   
    wire mult_en,div_en;
    reg  [31:0]hi,lo;
    reg  [4:0]count;

    assign mul_en=(aluop[`ALUOP_TYPE_BASICOP_LEN-1:0]==`ALUOP_TYPE_MULT);
    assign div_en=(aluop[`ALUOP_TYPE_BASICOP_LEN-1:0]==`ALUOP_TYPE_DIV);

    assign busy=(count==0);
    assign data=addr?hi:lo;

    
    always@(posedge rst or posedge clk)
    begin
        if(rst)
        begin
            hi<=0;
            lo<=0;
            count=0;
        end
        else    //clock
        begin
            if(!busy)
            begin
                if(mul_en)
                begin
                    count<=`MULT_CLOCK;
                end
                else if(div_en)
                begin
                    count<=`DIV_CLOCK;
                end
                else if(wr_en)
                begin
                    if(addr)hi<=wdata;
                    else lo<=wdata;
                end

            end
            else//busy
            begin
                count<=(count==0)?0:(count-1);
            end
        end
    end

endmodule

module multdiv
(
    //input
    input clk,
    input rst,
    input [`ALUOP_TYPE_LEN-1:0]aluop,

    input [31:0]src1op_data,
    input [31:0]src2op_data,

    input [31:0]dstop_type,

    //output
    output busy,
    output [31:0]dstop_data
    );

    wire wr_en;
    wire addr;//0:lo 1:hi
    wire [31:0]wdata;
   
    assign wr_en=((dstop_type==`OPERAND_TYPE_HI)||(dstop_type==`OPERAND_TYPE_LO));
    assign addr=(dstop_type==`OPERAND_TYPE_HI)?1:0;
    assign wdata=src1op_data;
    
    _mult _Mult
    (
        .clk(clk),
        .rst(rst),
        .aluop(aluop),
        
        .rs_data(src1op_data),
        .rt_data(src2op_data),

        .wr_en(wr_en),
        .addr(addr),
        .wdata(wdata),
        .busy(busy),

        .data(dstop_data)

    );

endmodule
*/

module mult(
    input clk,
    input rst,
    input mult_en, 
    input mult_signed,
    input [31:0]A,
    input [31:0]B,

    //to mult_div
    output reg mult_busy,
    output [31:0]prodHI,
    output [31:0]prodLO
);

wire [31:0]abs_A=(mult_signed&&(A[31]))?(~A+32'b1):A;
wire [31:0]abs_B=(mult_signed&&(B[31]))?(~B+32'b1):B;
wire [63:0]abs_P;

wire [63:0]P=(mult_signed && (A[31]!=B[31]))?(~abs_P + 64'b1):abs_P;

reg [5:0]count;

assign prodHI=P[63:32];
assign prodLO=P[31:0];


mult_gen_0 your_instance_name (
  .CLK(clk),  // input wire CLK
  .A(abs_A),      // input wire [31 : 0] A
  .B(abs_B),      // input wire [31 : 0] B
  .P(abs_P)      // output wire [63 : 0] P
);




always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        mult_busy<=0;
        count<=0;
    end
    else
    begin
        if(count==0)
        begin
            if(mult_en )
            begin
                count<=`MULT_CLOCK-1;
                mult_busy<=1;
            end
            else
            begin
                count<=0;
                mult_busy<=0;
            end
        end
        else
        count<=count-1;
    end
end

endmodule
