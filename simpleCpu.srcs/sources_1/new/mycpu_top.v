
/*
    .clk              (cpu_clk   ),
.resetn           (cpu_resetn),  //low active
.int              (6'd0      ),  //interrupt,high active

.inst_sram_en     (cpu_inst_en   ),
.inst_sram_wen    (cpu_inst_wen  ),
.inst_sram_addr   (cpu_inst_addr ),
.inst_sram_wdata  (cpu_inst_wdata),
.inst_sram_rdata  (cpu_inst_rdata),

.data_sram_en     (cpu_data_en   ),
.data_sram_wen    (cpu_data_wen  ),
.data_sram_addr   (cpu_data_addr ),
.data_sram_wdata  (cpu_data_wdata),
.data_sram_rdata  (cpu_data_rdata),

//debug
.debug_wb_pc      (debug_wb_pc      ),
.debug_wb_rf_wen  (debug_wb_rf_wen  ),
.debug_wb_rf_wnum (debug_wb_rf_wnum ),
.debug_wb_rf_wdata(debug_wb_rf_wdata)
*/

/*
//cpu inst sram
wire        cpu_inst_en;
wire [3 :0] cpu_inst_wen;
wire [31:0] cpu_inst_addr;
wire [31:0] cpu_inst_wdata;
wire [31:0] cpu_inst_rdata;
//cpu data sram
wire        cpu_data_en;
wire [3 :0] cpu_data_wen;
wire [31:0] cpu_data_addr;
wire [31:0] cpu_data_wdata;
wire [31:0] cpu_data_rdata;

//data sram
wire        data_sram_en;
wire [3 :0] data_sram_wen;
wire [31:0] data_sram_addr;
wire [31:0] data_sram_wdata;
wire [31:0] data_sram_rdata;
*/

module mycpu_top(
    input clk,
    input resetn,
    input int,
    
    output inst_sram_en,
    output [3:0] inst_sram_wen,
    output [31:0]inst_sram_addr,
    output [31:0]inst_sram_wdata,
    input [31:0]inst_sram_rdata,
    
    output data_sram_en,
    output [3:0]data_sram_wen,
    output [31:0]data_sram_addr,
    output [31:0]data_sram_wdata,
    input  [31:0]data_sram_rdata,
    
    output [31:0]debug_wb_pc,
    output [3:0]debug_wb_rf_wen,
    output [4:0]debug_wb_rf_wnum,
    output [31:0]debug_wb_rf_wdata
    
    
    

    );
    
assign inst_sram_wdata=31'b0;
assign inst_sram_wen=4'b0;

wire [31:2]inst_sram_addr_30b,data_sram_addr_30b;
assign inst_sram_addr={inst_sram_addr_30b,2'b0};
assign data_sram_addr={data_sram_addr_30b,2'b0};

cpu_top Cpu_top(
    .clk(clk),
    .rst(~resetn),
    .iram_ena_o(inst_sram_en),
    .iram_addra_o(inst_sram_addr_30b),
    .iram_douta(inst_sram_rdata),
    
    .iram_stall(1'b0),
    
    .dram_ena_o(data_sram_en),
    .dram_addra_o(data_sram_addr_30b),
    .dram_wea_o(data_sram_wen),
    .dram_dina_o(data_sram_wdata),
    .dram_douta(data_sram_rdata),
    
    .dram_stall(1'b0),
    
    .debug_wb_pc_o(debug_wb_pc),
    .debug_wb_wen_o(debug_wb_rf_wen),
    .debug_wb_wnum_o(debug_wb_rf_wnum),
    .debug_wb_wdata_o(debug_wb_rf_wdata),
    

    .int(int)
    
);
endmodule
