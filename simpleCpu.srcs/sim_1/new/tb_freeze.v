`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/24/2018 03:16:37 PM
// Design Name: 
// Module Name: tb_freeze
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Commenkkkkts:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defs.v"

`define debug_pass begin   \
        $display("pass");\
        $stop; end     

`define debug_fail begin   \
        $display("error:\tstd_pc:%h\tpc:%h",std_pc,pc);\
        $stop; end     

`define debug_assert(x) begin \
        if(!(x))`debug_fail;   \
        end;


module tb_freeze;
    reg [`OPERAND_TYPE_LEN-1:0]src2op_type,dstop_type,dstop_from_type,
        ex_dstop_type,mem1_dstop_type,mem2_dstop_type,wb_dstop_type;
       
    reg [`OPERAND_ADDR_LEN-1:0]src1op_addr,src2op_addr,dstop_addr,
         ex_dstop_addr,mem1_dstop_addr,mem2_dstop_addr,wb_dstop_addr;   

endmodule
