module stage_if2(
    input clk,
    input rst,
    input stall,
    input flush,

    //from if1
    input [31:0]pc,
    input [31:0]pca4,

    //from iram->cpu
    input [31:0]iram_douta,
    //input [31:0]inst,  

    output [31:0]inst_o,
    output [31:0]pca4_o,
    output [31:0]pc_o
    
);

//if1->if2
reg [31:0]if1_if2_pca4,if1_if2_pc;
wire [31:0]inst;

reg inst_valid;



assign pca4_o=if1_if2_pca4;
assign inst=iram_douta;
assign inst_o=((~inst_valid)|stall|flush)?`INST_NONE:inst;
assign pc_o=if1_if2_pc;


always@(posedge clk or posedge rst)
begin
    if(rst)
    begin
        if1_if2_pca4<=0;
        if1_if2_pc<=0;
        inst_valid<=0;
    end
    else //clk
        if(stall);
        else
        begin
            if1_if2_pca4<=pca4;
            if1_if2_pc<=pc;
            inst_valid<=1;
        end
end

endmodule
