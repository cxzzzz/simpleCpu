`include "../defs.v"



module id(
    //input
    input [31:0]inst,

    //output_common
//    output reg[2:0]src1op_t_use, 
//    output reg[2:0]src2op_t_use, 
//    output reg[2:0]dstop_t_new,
    
    output reg[`OPERAND_ADDR_LEN-1:0]src1op_addr,
    output reg[`T_LEN-1:0]src1op_tuse,

    output reg[`OPERAND_TYPE_LEN-1:0]src2op_type,
    output reg[`OPERAND_ADDR_LEN-1:0]src2op_addr,
    output reg[`T_LEN-1:0]src2op_tuse,

    //output reg dstop_wren, 
    output reg[`OPERAND_TYPE_LEN-1:0]dstop_type,dstop_from_type,
    output reg[`OPERAND_ADDR_LEN-1:0]dstop_addr,dstop_from_addr,
    output reg[`T_LEN-1:0]dstop_from_tuse,

    //ALUOP_type
    output reg[`ALUOP_TYPE_LEN-1:0]aluop,
    //MEMOP_type
    output reg[`MEMOP_TYPE_LEN-1:0]memop,

    //output_j
    output reg[`JADDR_TYPE_LEN-1:0]jaddr_type,

    //output_exception
    output reg[4:0]exc_code,
    output reg exc_en
    //output reg branch_en //for cp0


    );
    
    wire [5:0]inst_31_26=inst[31:26];
    wire [4:0]inst_25_21=inst[25:21];
    wire [4:0]inst_20_16=inst[20:16];
    wire [4:0]inst_15_11=inst[15:11];
    wire [4:0]inst_10_6=inst[10:6];
    wire [5:0]inst_5_0=inst[5:0];
    wire [15:0]inst_15_0=inst[15:0];
    
    wire [8:0]c0_addr={inst[15:11]};//,inst[2:0]};
    
    
    always@(*)
    begin
        src1op_addr=0;
        src1op_tuse=7;
       
        src2op_type=`OPERAND_TYPE_NONE;
        src2op_addr=0;
        src2op_tuse=7;
        
        dstop_type=`OPERAND_TYPE_NONE;
        dstop_from_type=`OPERAND_TYPE_NONE;
        dstop_addr=0;
        dstop_from_addr=0;
        dstop_from_tuse=7;
        
        aluop=`ALUOP_TYPE_NONE;
        memop=`MEMOP_TYPE_NONE;
        
        jaddr_type=`JADDR_TYPE_NONE;
        
        exc_code=0;
        exc_en=0;
        
        casez(inst)
            //add
             32'b000000_?????_?????_?????_00000_100000:
             begin
                 aluop=`ALUOP_TYPE_ADD|`ALUOP_TYPE_EXCEPTION;
                 src1op_addr=inst_25_21;
                 src2op_type=`OPERAND_TYPE_REG;
                 src2op_addr=inst_20_16;
                 
                 dstop_from_type=`OPERAND_TYPE_ALU;
                 dstop_type=`OPERAND_TYPE_REG;
                 dstop_addr=inst_15_11;
                 
                 src1op_tuse=1;
                 src2op_tuse=1;                
             end
             
            //addu
            32'b000000_?????_?????_?????_00000_100001:
            begin
                aluop=`ALUOP_TYPE_ADD;
                src1op_addr=inst_25_21;
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
                src1op_tuse=1;
                src2op_tuse=1;
            end
            
            //addi
            32'b001000_?????_?????_?????_?????_??????:
            begin
                aluop=`ALUOP_TYPE_ADD|`ALUOP_TYPE_EXCEPTION;
                src1op_addr=inst_25_21;
                src2op_type=`OPERAND_TYPE_IMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                
                src1op_tuse=1;
            end
            
            //addiu
            32'b001001_?????_?????_?????_?????_??????:
            begin
                aluop=`ALUOP_TYPE_ADD;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                

            end
            
            
            //sub
            32'b000000_?????_?????_?????_00000_100010:
            begin
                aluop=`ALUOP_TYPE_ADD|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_ADDONE|`ALUOP_TYPE_EXCEPTION;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            //subu
            32'b000000_?????_?????_?????_00000_100011:
            begin
                aluop=`ALUOP_TYPE_ADD|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_ADDONE;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            //slt
            32'b000000_?????_?????_?????_00000_101010:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED;
                
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            //slti
            32'b001010_?????_?????_?????_?????_??????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                
                src1op_tuse=1;
                src2op_tuse=1;
            end
            
            //sltu
            32'b000000_?????_?????_?????_00000_101011:
            begin
                aluop=`ALUOP_TYPE_CMP;
                
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            //sltiu
            32'b001011_?????_?????_?????_?????_??????:
            begin
                aluop=`ALUOP_TYPE_CMP;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                
                src1op_tuse=1;
                src2op_tuse=1;
            end
            
            
            //and
            32'b000000_?????_?????_?????_00000_100100:
            begin
                aluop=`ALUOP_TYPE_OR|`ALUOP_TYPE_OP1NOR|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_OP3REV;
                
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            //andi
            32'b001100_?????_?????_?????_?????_??????:
            begin
                aluop=`ALUOP_TYPE_OR|`ALUOP_TYPE_OP1NOR|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_OP3REV;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_UIMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                
                src1op_tuse=1;
                src2op_tuse=1;
            end
            
            
            //nor
            32'b000000_?????_?????_?????_00000_100111:
            begin
                aluop=`ALUOP_TYPE_OR|`ALUOP_TYPE_OP3REV;
                
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            
            //or
            32'b000000_?????_?????_?????_00000_100101:
            begin
                aluop=`ALUOP_TYPE_OR;
                
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            
            //ori
            32'b001101_?????_?????_?????_?????_??????:
            begin
                aluop=`ALUOP_TYPE_OR;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_UIMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;

            end
            
            //xor

            32'b000000_?????_?????_?????_00000_100110:
            begin
                aluop=`ALUOP_TYPE_XOR;
                
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
            end
            
            //xori
            32'b001110_?????_?????_?????_?????_??????:
            begin
                aluop=`ALUOP_TYPE_XOR;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_UIMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;

            end    
            
            
            //lui
            32'b001111_00000_?????_????????????????:
            begin
                aluop=`ALUOP_TYPE_LUI;
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_UIMME;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;    
                            
            end
            
            
            //sllv
            32'b000000_?????_?????_?????_00000_000100:
            begin
                aluop=`ALUOP_TYPE_SHIFT_LEFT;
                
                src1op_addr=inst_20_16;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_25_21;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;    
            end
            
            //sll
            32'b00000_00000_?????_?????_?????_000000:
            begin
                aluop=`ALUOP_TYPE_SHIFT_LEFT;
                src1op_addr=inst_20_16;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_SHIFT;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;        
            end
            
            //srav
            32'b000000_?????_?????_?????_00000_000111:
            begin
                aluop=`ALUOP_TYPE_SHIFT_RIGHT|`ALUOP_TYPE_SHIFT_ARITHMETIC;
                
                src1op_addr=inst_20_16;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_25_21;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;    
            end
            
             //sra
            32'b00000_00000_?????_?????_?????_000011:
            begin
                aluop=`ALUOP_TYPE_SHIFT_RIGHT|`ALUOP_TYPE_SHIFT_ARITHMETIC;
                src1op_addr=inst_20_16;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_SHIFT;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;        
            end
                        
            //srlv
            32'b000000_?????_?????_?????_00000_000110:
            begin
                aluop=`ALUOP_TYPE_SHIFT_RIGHT;
                
                src1op_addr=inst_20_16;
                src1op_tuse=1;
                
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_25_21;
                src2op_tuse=1;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;    
            end
            
             //srl
            32'b00000_00000_?????_?????_?????_000010:
            begin
                aluop=`ALUOP_TYPE_SHIFT_RIGHT;
                src1op_addr=inst_20_16;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_SHIFT;
                
                dstop_from_type=`OPERAND_TYPE_ALU;
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;        
            end
                   
            
            //beq
            32'b000100_?????_?????_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_EQU;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=0;
                
                jaddr_type=`JADDR_TYPE_B;
                
            end
            
            //bne
            32'b000101_?????_?????_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_EQU|`ALUOP_TYPE_OP3REV;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=0;
                
                jaddr_type=`JADDR_TYPE_B;
                
            end
            
            //bgez
            32'b000001_?????_00001_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_OP3REV|`ALUOP_TYPE_SIGNED;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_NONE;
                
                jaddr_type=`JADDR_TYPE_B;
                
            end
            
            //bgtz
            32'b000111_?????_00000_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_OP1NOR|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_SIGNED;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_NONE;
                jaddr_type=`JADDR_TYPE_B;
                
            end
            
            //blez
            32'b000110_?????_00000_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_OP1NOR|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_OP3REV|`ALUOP_TYPE_SIGNED;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_NONE;
                
                jaddr_type=`JADDR_TYPE_B;
                
            end            
            
            //bltz
            32'b000001_?????_00000_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_NONE;
                
                jaddr_type=`JADDR_TYPE_B;
                
            end
            
            //bgezal
            32'b000001_?????_10001_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_OP3REV|`ALUOP_TYPE_SIGNED;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_NONE;
                
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=`OPERAND_ADDR_LEN'd31;
                dstop_from_type=`OPERAND_TYPE_PC;
                dstop_from_tuse=4;
                
                jaddr_type=`JADDR_TYPE_B;
                
            end
            
            //bltzal
            32'b000001_?????_10000_????????????????:
            begin
                aluop=`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED;
                src1op_addr=inst_25_21;
                src1op_tuse=0;
                
                src2op_type=`OPERAND_TYPE_NONE;
                
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=`OPERAND_ADDR_LEN'd31;
                dstop_from_type=`OPERAND_TYPE_PC;
                dstop_from_tuse=4;
                
                jaddr_type=`JADDR_TYPE_B;
                
            end
            
            
            //j
            32'b000010_?????_?????_????????????????:
            begin
               jaddr_type=`JADDR_TYPE_J;
            end
            
            //jal
            32'b000011_?????_?????_????????????????:
            begin
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=`OPERAND_ADDR_LEN'd31;
                dstop_from_type=`OPERAND_TYPE_PC;
                dstop_from_tuse=4;
                
               jaddr_type=`JADDR_TYPE_J;
            end
            
            //jr 
            32'b000000_?????_00000_00000_00000_001000:
            begin
               src1op_addr=inst_25_21;
               src1op_tuse=0;
               jaddr_type=`JADDR_TYPE_JR;
            end
            
            
            //jalr
            32'b000000_?????_00000_?????_00000_001001:
            begin
               src1op_addr=inst_25_21;
               src1op_tuse=0;
               
               dstop_type=`OPERAND_TYPE_REG;
               dstop_addr=inst_15_11;
               dstop_from_type=`OPERAND_TYPE_PC;
               dstop_from_tuse=4;
               
               jaddr_type=`JADDR_TYPE_JR;
            end 
            
            
            //multu
            32'b000000_?????_?????_00000_00000_011001:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                aluop=`ALUOP_TYPE_MULT;
            end
            
            //mult
            32'b000000_?????_?????_00000_00000_011000:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                aluop=`ALUOP_TYPE_MULT|`ALUOP_TYPE_SIGNED;
            end
            
            
            
            //madd
            32'b011100_?????_?????_00000_00000_000000:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                aluop=`ALUOP_TYPE_MULT|`ALUOP_TYPE_MDADD|`ALUOP_TYPE_SIGNED;
            
            end
            
            //maddu
            32'b011100_?????_?????_00000_00000_000001:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                aluop=`ALUOP_TYPE_MULT|`ALUOP_TYPE_MDADD;
            end
                        
            //div
            32'b000000_?????_?????_00000_00000_011010:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                aluop=`ALUOP_TYPE_DIV|`ALUOP_TYPE_SIGNED;
            end            
            
            
            //divu
            32'b000000_?????_?????_00000_00000_011011:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_REG;
                src2op_addr=inst_20_16;
                src2op_tuse=1;
                
                aluop=`ALUOP_TYPE_DIV;
            end            
            
                        
            //mfhi
            32'b000000_0000000000_?????_00000_010000:
            begin
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                
                dstop_from_type=`OPERAND_TYPE_HI;
                //dstop_from_tuse=4;
            end
            
            //mflo
            32'b000000_0000000000_?????_00000_010010:
            begin
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_15_11;
                dstop_from_type=`OPERAND_TYPE_LO;
                //dstop_from_tuse=4;
            end
                        
            
            //mthi
            32'b000000_?????_00000_00000_00000_010001:
            begin
                dstop_type=`OPERAND_TYPE_HI;
                
                dstop_from_type=`OPERAND_TYPE_REG;  
                dstop_from_addr=inst_25_21;
                dstop_from_tuse=4;          
            end
            
            //mtlo
            32'b000000_?????_00000_00000_00000_010011:
            begin
                dstop_type=`OPERAND_TYPE_LO;
                
                dstop_from_type=`OPERAND_TYPE_REG;  
                dstop_from_addr=inst_25_21;
                dstop_from_tuse=4;          
            end       
            
            //mfc0
            32'b010000_00000_?????_?????_00000000_???:
            begin
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                
                dstop_from_type=`OPERAND_TYPE_CP0;
                dstop_from_addr=c0_addr;
            end
            
            //mtc0     
            32'b010000_00100_?????_?????_00000000_???:
            begin
                dstop_type=`OPERAND_TYPE_CP0;
                dstop_addr=c0_addr;
                
                dstop_from_type=`OPERAND_TYPE_REG;
                dstop_from_addr=inst_20_16;
            end
                        
            
            
            //syscall
            32'b000000_?????_?????_?????_?????_001100:
            begin
                exc_code=`EXCEPTION_TYPE_SYSCALL;
                exc_en=1;
            end
            
            //break
            32'b000000_?????_?????_?????_?????_001101:
            begin
                exc_code=`EXCEPTION_TYPE_BP;
                exc_en=1;
            end
            
            //eret
            32'b010000_10000_00000_00000_00000_011000:
            begin
                exc_code=`EXCEPTION_TYPE_ERET;
                exc_en=1;                
            end
            
            
            //lw
            32'b100011_?????_?????_?????_?????_??????:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                src2op_tuse=1;
                
                //dstop_from_type=`OPERAND_TYPE_MEM;
                
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                dstop_from_type=`OPERAND_TYPE_MEM;
                
                aluop=`ALUOP_TYPE_ADD;
                
                memop=`MEMOP_TYPE_READ|`MEMOP_TYPE_WORD;
                
            end
            
            //lb
            32'b100000_?????_?????_?????_?????_??????:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                src2op_tuse=1;
                
                //dstop_from_type=`OPERAND_TYPE_MEM;
                
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                dstop_from_type=`OPERAND_TYPE_MEM;
                
                aluop=`ALUOP_TYPE_ADD;
                
                memop=`MEMOP_TYPE_READ|`MEMOP_TYPE_BYTE|`MEMOP_TYPE_SIGNED;
            end
            
            //lbu
            32'b100100_?????_?????_?????_?????_??????:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                src2op_tuse=1;
                
                //dstop_from_type=`OPERAND_TYPE_MEM;
                
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                dstop_from_type=`OPERAND_TYPE_MEM;
                
                aluop=`ALUOP_TYPE_ADD;
                
                memop=`MEMOP_TYPE_READ|`MEMOP_TYPE_BYTE|`MEMOP_TYPE_UNSIGNED;
            end
            
            //lh
            32'b100001_?????_?????_?????_?????_??????:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                src2op_tuse=1;
                
                //dstop_from_type=`OPERAND_TYPE_MEM;
                
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                dstop_from_type=`OPERAND_TYPE_MEM;
                
                aluop=`ALUOP_TYPE_ADD;
                
                memop=`MEMOP_TYPE_READ|`MEMOP_TYPE_HALF|`MEMOP_TYPE_SIGNED;
            end
            
            //lhu
            32'b100101_?????_?????_?????_?????_??????:
            begin
                src1op_addr=inst_25_21;
                src1op_tuse=1;
                
                src2op_type=`OPERAND_TYPE_IMME;
                src2op_tuse=1;
                
                //dstop_from_type=`OPERAND_TYPE_MEM;
                
                dstop_type=`OPERAND_TYPE_REG;
                dstop_addr=inst_20_16;
                dstop_from_type=`OPERAND_TYPE_MEM;
                
                aluop=`ALUOP_TYPE_ADD;
                
                memop=`MEMOP_TYPE_READ|`MEMOP_TYPE_HALF|`MEMOP_TYPE_UNSIGNED;
            end
            
            //sb
            32'b101000_?????_?????_?????_?????_??????:
             begin
                           src1op_addr=inst_25_21;
                           src1op_tuse=1;
                           
                           src2op_type=`OPERAND_TYPE_IMME;
                           src2op_tuse=1;
                           
                           //dstop_from_type=`OPERAND_TYPE_MEM;
                           
                           dstop_from_type=`OPERAND_TYPE_REG;
                           dstop_from_addr=inst_20_16;
                           dstop_from_tuse=2;
                           
                           dstop_type=`OPERAND_TYPE_MEM;
                           
                           aluop=`ALUOP_TYPE_ADD;
                           
                           memop=`MEMOP_TYPE_WRITE|`MEMOP_TYPE_BYTE;
             end
             //sh
              32'b101001_?????_?????_?????_?????_??????:
              begin
                            src1op_addr=inst_25_21;
                            src1op_tuse=1;
                            
                            src2op_type=`OPERAND_TYPE_IMME;
                            src2op_tuse=1;
                            
                            //dstop_from_type=`OPERAND_TYPE_MEM;
                            
                            dstop_from_type=`OPERAND_TYPE_REG;
                            dstop_from_addr=inst_20_16;
                            dstop_from_tuse=2;
                            
                            dstop_type=`OPERAND_TYPE_MEM;
                            
                            aluop=`ALUOP_TYPE_ADD;
                            
                            memop=`MEMOP_TYPE_WRITE|`MEMOP_TYPE_HALF;
              end
              
               //sw
               32'b101011_?????_?????_?????_?????_??????:
               begin
                             src1op_addr=inst_25_21;
                             src1op_tuse=1;
                             
                             src2op_type=`OPERAND_TYPE_IMME;
                             src2op_tuse=1;
                             
                             //dstop_from_type=`OPERAND_TYPE_MEM;
                             
                             dstop_from_type=`OPERAND_TYPE_REG;
                             dstop_from_addr=inst_20_16;
                             dstop_from_tuse=2;
                             
                             dstop_type=`OPERAND_TYPE_MEM;
                             
                             aluop=`ALUOP_TYPE_ADD;
                             
                             memop=`MEMOP_TYPE_WRITE|`MEMOP_TYPE_WORD;
               end
             
            
        endcase     
    end

    endmodule
