//`include "defs.h"

module tb_alu;

reg [`ALUOP_TYPE_LEN-1:0]aluop;
reg [31:0]src1op_data,src2op_data;
wire [31:0]dstop_data;
wire exc_en;
wire [4:0]exc_code;
alu Alu(
    .aluop(aluop),
    .src1op_data(src1op_data),
    .src2op_data(src2op_data),
    .dstop_data(dstop_data),
    .exc_en(exc_en),
    .exc_code(exc_code)
);

reg [31:0]stand_dstop_data;
reg stand_exc_en;
reg [4:0] stand_exc_code;
reg [8*7:0] test_type;

`define debug_fail begin   \
        $display("error:aluop:%b",aluop);\
        $display("op1:%h\top2:%h",src1op_data,src2op_data); \
        $display("res:%h\tstd_res:%h",dstop_data,stand_dstop_data); \
        $display("exc_en:%h\tstd_exc_en:%h",exc_en,stand_exc_en);   \
        $display("exc_code:%h\tstd_exc_code:%h",exc_code,stand_exc_code);\
        $stop; end     

`define debug_begin \
        $display("test:%s begining",test_type);
`define debug_pass \
        $display("test:%s passed\n\n",test_type);

`define test( _aluop_str,_aluop,_op,_exc_en,_exc_code,_times) \
    aluop=_aluop;   \
    test_type=_aluop_str; \
    `debug_begin    \
    for(i=0;i<_times;i=i+1) \
    begin   \
        src1op_data=$random;    \
        src2op_data=$random;    \
        stand_dstop_data=_op;   \
        #1; \
        stand_exc_en=_exc_en;   \
        stand_exc_code=_exc_code;   \
        #1;      \
        if(stand_dstop_data!=dstop_data)    \
            `debug_fail     \
        if(exc_en!=stand_exc_en)    \
            `debug_fail     \
        if(stand_exc_en==1'b1 && (exc_code!=stand_exc_code))  \
            `debug_fail     \
    end     \
    `debug_pass

`define addi_exc_en ^(({{src1op_data[31]},src1op_data}+{{src2op_data[31]},src2op_data})[32:31])

    wire add_ov,sub_ov;
    wire [32:0]add_ov_tmp,sub_ov_tmp;
    wire [31:0] slt;
    assign add_ov_tmp={src1op_data[31],src1op_data}+{src2op_data[31],src2op_data};
    assign add_ov=(add_ov_tmp[32]==add_ov_tmp[31])?0:1;
    assign sub_ov_tmp={src1op_data[31],src1op_data}-{src2op_data[31],src2op_data};
    assign sub_ov=(sub_ov_tmp[32]==sub_ov_tmp[31])?0:1;

    assign slt=($signed(src1op_data)<$signed(src2op_data))?32'b1:32'b0;
integer i;
initial
begin
    //add
    `test("addu",`ALUOP_TYPE_ADD,src1op_data+src2op_data, 0,5'b1,200);
    `test("subu",`ALUOP_TYPE_ADD|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_ADDONE,src1op_data-src2op_data, 0,5'b1,200);
    
    `test("add",`ALUOP_TYPE_ADD|`ALUOP_TYPE_SIGNED|`ALUOP_TYPE_EXCEPTION,src1op_data+src2op_data,add_ov,`EXCEPTION_TYPE_OV,100);
    `test("sub",`ALUOP_TYPE_ADD|`ALUOP_TYPE_ADDONE|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_SIGNED|`ALUOP_TYPE_EXCEPTION,src1op_data-src2op_data,sub_ov,`EXCEPTION_TYPE_OV,100);

    `test("sltu",`ALUOP_TYPE_CMP,src1op_data<src2op_data, 0,5'b1,200);
    `test("slt",`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED,($signed(src1op_data)<$signed(src2op_data))?32'b1:32'b0, 0,5'b1,200);
    `test("and",`ALUOP_TYPE_OR|`ALUOP_TYPE_OP1NOR|`ALUOP_TYPE_OP2NOR|`ALUOP_TYPE_OP3REV,src1op_data&src2op_data, 0,5'b1,200);
    `test("or",`ALUOP_TYPE_OR,src1op_data|src2op_data, 0,5'b1,200);
    `test("nor",`ALUOP_TYPE_OR|`ALUOP_TYPE_OP3REV,~(src1op_data|src2op_data), 0,5'b1,200);
    `test("xor",`ALUOP_TYPE_XOR,src1op_data^src2op_data, 0,5'b1,200);
    `test("lui",`ALUOP_TYPE_LUI,src2op_data<<16, 0,5'b1,200);

    `test("sll",`ALUOP_TYPE_SHIFT_LEFT,src1op_data<<src2op_data[4:0], 0,5'b1,200);
    `test("srl",`ALUOP_TYPE_SHIFT_RIGHT,src1op_data>>src2op_data[4:0], 0,5'b1,200);
    `test("sra",`ALUOP_TYPE_SHIFT_RIGHT|`ALUOP_TYPE_SHIFT_ARITHMETIC,src1op_data>>>src2op_data[4:0], 0,5'b1,200);
    //`test("slt",(`ALUOP_TYPE_CMP|`ALUOP_TYPE_SIGNED),($signed)src1op_data<($signed)src2op_data, 0,5'b1,100);
end
endmodule
