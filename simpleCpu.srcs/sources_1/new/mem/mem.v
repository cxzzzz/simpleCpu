`include "../defs.v"

module mem1(
    

    input [31:0]addr,
    input [`MEMOP_TYPE_LEN-1:0]memop,
    input [31:0]dstop_data,
    input [`OPERAND_TYPE_LEN-1:0]dstop_type,
    input [`OPERAND_TYPE_LEN-1:0]dstop_from_type,


    output dram_ena,
    output reg[3:0]dram_wea,
    output [31:2]dram_addra,
    output [31:0]dram_dina,

    output [4:0]exc_code,
    output reg exc_en
    
    );

    reg [31:2]old_addr;
    reg [`MEMOP_TYPE_LEN-1:0]old_memop;
    reg [31:0]old_dstop_data;
    

    assign dram_ena=(dstop_type==`OPERAND_TYPE_MEM || dstop_from_type==`OPERAND_TYPE_MEM);


    assign dram_addra=addr[31:2];

    assign dram_dina=((dstop_data)<<({addr[1:0],3'b0}));

   // assign dram_wea=(dstop_type==`OPERAND_TYPE_MEM)?4'b1:4'b0;  //just for test
    assign exc_code=(memop&`MEMOP_TYPE_RW_MASK)?`EXCEPTION_TYPE_ADES:`EXCEPTION_TYPE_ADEL;
    

    always@(*)
    begin
        exc_en=0;
        if(dram_ena)
            case(memop&`MEMOP_TYPE_LEN_MASK)
                `MEMOP_TYPE_WORD:
                    if(addr[1:0]!=2'b00)exc_en=1;
                `MEMOP_TYPE_HALF:
                    if(addr[0]!=0)exc_en=1;
            endcase
    end


    always@(*)
    begin
        dram_wea=0;
        if(exc_en || (memop&`MEMOP_TYPE_RW_MASK)==`MEMOP_TYPE_READ)
            dram_wea=4'b0;
        else
        begin
            case (memop&`MEMOP_TYPE_LEN_MASK)
               `MEMOP_TYPE_WORD:
                    dram_wea=4'b1111;
               `MEMOP_TYPE_HALF:
                    dram_wea=addr[1]?4'b1100:4'b0011;
                `MEMOP_TYPE_BYTE:
                    dram_wea=(4'b1)<<addr[1:0];
            endcase
        end
    end

endmodule

module mem2(
    input [31:0]dram_douta,
    //input [3:0]dstop_addr,
    //input [3:0]dstop_type,

    input [31:0]addr,
    input [`MEMOP_TYPE_LEN-1:0]memop,

    output reg[31:0]dstop_data
);
    //output reg[31:0]dstop_unsigned_data;
    always@(*)
    begin
        dstop_data=0;
        case(memop&`MEMOP_TYPE_LEN_MASK)
            `MEMOP_TYPE_WORD:
                dstop_data=dram_douta;
            `MEMOP_TYPE_HALF:   
            begin
                case(memop&`MEMOP_TYPE_SIGN_MASK)
                    `MEMOP_TYPE_SIGNED:
                        dstop_data=addr[1]?
                        {{16{dram_douta[31]}},dram_douta[31:16]}:
                        {{16{dram_douta[15]}},dram_douta[15:0]};
                    default:
                        dstop_data=addr[1]?
                        {16'b0,dram_douta[31:16]}:
                        {16'b0,dram_douta[15:0]};
                endcase
            end

            `MEMOP_TYPE_BYTE:
            begin
                case(memop&`MEMOP_TYPE_SIGN_MASK)
                    `MEMOP_TYPE_SIGNED:
                    begin
                        case(addr[1:0])
                            2'b00:
                                dstop_data={{27{dram_douta[7]}},dram_douta[7:0]}; 
                            2'b01:
                                dstop_data={{27{dram_douta[15]}},dram_douta[15:8]}; 
                            2'b10:
                                dstop_data={{27{dram_douta[23]}},dram_douta[23:16]}; 
                            default:
                                dstop_data={{27{dram_douta[31]}},dram_douta[31:24]}; 
                        endcase
                    end
                    default:
                    begin
                        case(addr[1:0])
                            2'b00:
                                dstop_data={27'b0,dram_douta[7:0]}; 
                            2'b01:
                                dstop_data={27'b0,dram_douta[15:8]}; 
                            2'b10:
                                dstop_data={27'b0,dram_douta[23:16]}; 
                            default:
                                dstop_data={27'b0,dram_douta[31:24]}; 
                        endcase
                    end
                endcase
            end
        endcase
    end

endmodule
